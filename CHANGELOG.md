# Changelog

## 1.2.0
* Removed unused educationscraping/dupe_filters.py

## 1.1.0 (Apr, 2024)

* [Update Uppsala universitet spider](https://gitlab.com/arbetsformedlingen/education/education-scraping/-/issues/20)
* [Update MYH-kvalifikationer spider](https://gitlab.com/arbetsformedlingen/education/education-scraping/-/issues/18)
* [Disable Skolverket educationplans spider](https://gitlab.com/arbetsformedlingen/education/education-scraping/-/issues/19)

## 1.0.4 (Jan, 2022)

* Packaging

## 1.0.0 (Sep, 2021)

* Initial release
