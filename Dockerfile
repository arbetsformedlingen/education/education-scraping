FROM docker.io/library/python:3.9.20-slim-bookworm

RUN apt-get -y update && apt-get -y install jq && apt-get clean
WORKDIR /educationscraping
COPY . /educationscraping
RUN pip install -r requirements.txt

CMD [ "python3", "/educationscraping/go_spiders_docker.py" ]
