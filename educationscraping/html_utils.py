from bs4 import BeautifulSoup
import re

tags_display_block = ['p', 'div', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']
max_number_of_linebreaks = 2
RE_MULTIPLE_LINEBREAKS = re.compile(r"\n{" + str(max_number_of_linebreaks + 1) + ",}")

# Extracts the CDATA part (i.e. originally from XML).
def clean_cdata(text):
    if text is None:
        return text
    cdata = ''
    soup = BeautifulSoup(text, 'html.parser')
    for cd in soup.findAll(text=True):
        cdata += cd
    # Trim leading and ending spaces...
    cdata = cdata.strip()
    if cdata != '':
        return cdata
    else:
        return text

def clean_html(text):
    if text is None:
        cleaned_text = ''
    else:
        soup = BeautifulSoup(text, 'html.parser')
        # Remove all script-tags
        [s.extract() for s in soup('script')]

        for tag_name in tags_display_block:
            _add_linebreak_for_tag_name(tag_name, '\n', '\n', soup)

        _add_linebreak_for_tag_name('li', '', '\n', soup)
        _add_linebreak_for_tag_name('br', '', '\n', soup)

        cleaned_text = soup.get_text()
        cleaned_text = limit_linebreaks(cleaned_text)
        cleaned_text = cleaned_text.strip()

    return cleaned_text


def _add_linebreak_for_tag_name(tag_name, replacement_before, replacement_after, soup):
    parent_tags = soup.find_all(tag_name, recursive=True)

    for tag in parent_tags:
        if replacement_before:
            tag.insert_before(replacement_before)
        if replacement_after:
            tag.insert_after(replacement_after)


def limit_linebreaks(text):
    '''
    Limits to max number of linebreaks
    '''
    limited_text = text.replace('\r', '\n')
    limited_text = RE_MULTIPLE_LINEBREAKS.sub("\n" * max_number_of_linebreaks, limited_text)
    return limited_text
