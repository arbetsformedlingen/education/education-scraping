# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# Main item:
class Education(scrapy.Item):
    education = scrapy.Field() # EducationItem[]
    events = scrapy.Field() # EventItem[]
    providers = scrapy.Field() # EducationProviderItem[]

class Location(scrapy.Item):
    town = scrapy.Field()
    streetAddress = scrapy.Field()
    postBox = scrapy.Field() # number
    postCode = scrapy.Field()
    country = scrapy.Field()
    municipalityCode = scrapy.Field()
    regionCode = scrapy.Field()

class LangContent(scrapy.Item):
    lang = scrapy.Field()
    content = scrapy.Field()

class CodeType(scrapy.Item):
    code = scrapy.Field()
    type = scrapy.Field()

class Credits(scrapy.Item):
    system = scrapy.Field() # CodeType []
    credits = scrapy.Field() # number []

class Execution(scrapy.Item):
    condition = scrapy.Field() # number
    start = scrapy.Field() # 2021-11-08T00:00:00.000+01:00
    end = scrapy.Field() # 2022-01-16T00:00:00.000+01:00

class TuitionFee(scrapy.Item):
    total = scrapy.Field() # number []
    content = scrapy.Field() # boolean
    first = scrapy.Field() # number []

class StartPeriod(scrapy.Item):
    periodNumber = scrapy.Field() # number
    year = scrapy.Field() # number
    vacation = scrapy.Field() # string
    week = scrapy.Field() # string

class ApplicationDetails(scrapy.Item):
    closeDate = scrapy.Field() # 2021-04-15
    applicationModel = scrapy.Field()
    visibleToInternationalApplicants = scrapy.Field() # boolean
    onlyAsPartOfPackage = scrapy.Field() # boolean
    visibleToSwedishApplicants = scrapy.Field() # boolean
    onlyAsPartOfProgram = scrapy.Field() # boolean

class Application(scrapy.Item):
    code = scrapy.Field()
    first = scrapy.Field()
    last = scrapy.Field() # Date "2021-06-30"
    instruction = scrapy.Field() # LangContent[]
    urls = scrapy.Field() # LangContent[]

class AdmissionDetails(scrapy.Item):
    selectionModel = scrapy.Field()
    credits = scrapy.Field() # Credits []
    credentialRatingModel = scrapy.Field()
    credentialRatingModelUseNoModel = scrapy.Field()
    eligibilityModelSB = scrapy.Field()
    eligibilityModelSBUseNoModel = scrapy.Field()

class AdmissionCredit(scrapy.Item):
    startWeek = scrapy.Field()
    stopWeek = scrapy.Field()
    year = scrapy.Field()
    vacation = scrapy.Field()
    content = scrapy.Field()

class Phone(scrapy.Item):
    number = scrapy.Field() # Str
    function = scrapy.Field() # LangContent

class AmountCurrency(scrapy.Item):
    amount = scrapy.Field()
    currency = scrapy.Field()

class Subject(scrapy.Item):
    code = scrapy.Field()
    name = scrapy.Field() # Subject name
    nameEn = scrapy.Field() # Subject name in english
    type = scrapy.Field()

class Distance(scrapy.Item):
    mandatory = scrapy.Field()
    optional = scrapy.Field()
    mandatoryNet = scrapy.Field()
    optionalNet = scrapy.Field()

class Eligibility(scrapy.Item):
    eligibilityDescription = scrapy.Field() # LangContent []
    additionDescription = scrapy.Field() # LangContent []

class EducationItem(scrapy.Item):
    # define the fields for your item here like:
    identifier = scrapy.Field()
    resultIsDegree = scrapy.Field()
    expires = scrapy.Field() # boolean
    recommendedPriorKnowledge = scrapy.Field() # LangContent
    code = scrapy.Field()
    configuration = scrapy.Field() # CodeType
    subject = scrapy.Field() # Subject []
    description = scrapy.Field() # LangContent []
    eligibility = scrapy.Field() # Eligibility
    lastEdited = scrapy.Field() # TODO Dateformat 2021-03-18T14:47:37.162+01:00
    title = scrapy.Field() # LangContent []
    isVocational = scrapy.Field() # Boolean
    form = scrapy.Field() # CodeType
    credits = scrapy.Field() # Credits
    educationLevel = scrapy.Field() # CodeType
    urls = scrapy.Field() # LangContent[]
    eligibleForStudentAid = scrapy.Field() # CodeType

class EventExtension(scrapy.Item):
    stopWeek = scrapy.Field() # String
    keywords = scrapy.Field() # LangContent []
    tuitionFee = scrapy.Field() # TuitionFee
    itdistance = scrapy.Field() # boolean
    formOfFunding = scrapy.Field()
    startPeriod = scrapy.Field() # StartPeriod
    admissionDetails = scrapy.Field() # TODO make this part flatter or remove?
    educationEventRef = scrapy.Field() # String []
    uniqueIdentifier = scrapy.Field()
    applicationDetails = scrapy.Field() # ApplicationDetails
    id = scrapy.Field() # TODO Remove?
    type = scrapy.Field()
    eligibilityExemption = scrapy.Field()
    department = scrapy.Field()

class EventItem(scrapy.Item):
    identifier =  scrapy.Field()
    execution = scrapy.Field() # Execution
    expires = scrapy.Field() # 2022-09-30T00:00:00.000+02:00
    cancelled = scrapy.Field() # boolean
    education = scrapy.Field()
    distance = scrapy.Field() # Distance
    lastEdited = scrapy.Field()
    paceOfStudyPercentage = scrapy.Field()
    urls = scrapy.Field() # LangContent []
    application = scrapy.Field() # Application[]
    languageOfInstruction = scrapy.Field()
    provider = scrapy.Field()
    timeOfStudy = scrapy.Field() # CodeType
    locations = scrapy.Field() # Location[]
    fee = scrapy.Field() # AmountCurrency
    extension = scrapy.Field() # EventExtension

class EducationProviderItem(scrapy.Item):
    identifier = scrapy.Field()
    expires = scrapy.Field() #2031-03-17T16:59:44.071+01:00
    year = scrapy.Field() # CodeType []
    responsibleBody = scrapy.Field()
    responsibleBodyType = scrapy.Field() # CodeType
    emailAddress = scrapy.Field()
    phone = scrapy.Field() # Phone[]
    name = scrapy.Field() # LangContent[]
    contactAddress = scrapy.Field() # Location
    visitAddress = scrapy.Field() # Location
    urls = scrapy.Field()

# Example:

# {
#     "education": {
#         "identifier": [
#             "i.uoh.mdh.mma501.21171.20212",
#             "p.uoh.mdh"
#         ],
#         "resultIsDegree": false,
#         "expires": [
#             "2022-09-30T00:00:00.000+02:00",
#             "2031-03-17T16:59:44.071+01:00"
#         ],
#         "recommendedPriorKnowledge": {
#             "string": [
#                 {
#                     "lang": "swe",
#                     "content": "uh"
#                 }
#             ]
#         },
#         "code": "MMA501",
#         "configuration": {
#             "code": "kurs",
#             "type": "C_Configuration"
#         },
#         "subject": [
#             {
#                 "code": 389,
#                 "type": "UH_Subject"
#             }
#         ],
#         "description": [
#             {
#                 "lang": "swe",
#                 "content": "Algebra är en av de grundläggande områdena inom modern matematik. Den har sitt ursprung i talteori och geometri. Kursens syfte är att, genom exempel, hitta de matematiska strukturer och bakomliggande koncept inom talteori och geometri. Dessa strukturer, grupper, ringar och kroppar, tillämpas i flera sammanhang såsom uppräknings- och numreringsproblem, kodningsteori och kombinatorisk design."
#             },
#             {
#                 "lang": "eng",
#                 "content": "Algebra is one of the fundamental branches of modern mathematics. It has its origins in the theory of numbers and geometry. The aim of the course is to find, through examples, the mathematical structures underlying concepts in number theory and geometry. These structures, groups, rings and fields, are applied in multiple contexts such as counting and enumeration problems, coding theory and combinatorial designs."
#             }
#         ],
#         "eligibilityDescription": [
#             {
#                 "lang": "swe",
#                 "content": "120 hp i teknik, naturvetenskap, företagsekonomi eller nationalekonomi vari ingår minst två av kurserna Vektoralgebra 7,5 hp, Diskret matematik 7,5 hp, Linjär algebra 7,5 hp eller motsvarande. Dessutom krävs Svenska B/Svenska 3 samt Engelska A/Engelska 6. I de fall kursen ges på engelska görs undantag från kravet på Svenska B/Svenska 3."
#             },
#             {
#                 "lang": "eng",
#                 "content": "120 credit points in Engineering, Natural Science, Business Administration, or Economics, including at least two of the courses Vector Algebra 7.5 credit points, Discrete Mathematics 7.5 credit points, Linear Algebra 7.5 credit points, or the equivalent. In addition, Swedish B/Swedish 3 and English A/English 6 are required. In cases when the course is offered in English, the requirement for Swedish B/Swedish 3 is excluded. "
#             }
#         ],
#         "lastEdited": [
#             "2021-03-18T14:47:37.162+01:00",
#             "2021-03-17T16:59:44.071+01:00"
#         ],
#         "title": [
#             {
#                 "lang": "swe",
#                 "content": "Abstrakt algebra"
#             },
#             {
#                 "lang": "eng",
#                 "content": "Abstract Algebra"
#             }
#         ],
#         "form": {
#             "code": "högskoleutbildning",
#             "type": "C_OrganisationForm"
#         },
#         "credits": {
#             "system": [
#                 {
#                     "code": "hp",
#                     "type": "C_Credits"
#                 }
#             ],
#             "credits": [
#                 7.5
#             ]
#         },
#         "educationLevel": [
#             {
#                 "code": "avancerad",
#                 "type": "UH_EducationLevel"
#             }
#         ]
#     },
#     "event": {
#         "identifier": "e.uoh.mdh.mma501.21171.20212",
#         "execution": {
#             "condition": 1,
#             "start": "2021-11-08T00:00:00.000+01:00",
#             "end": "2022-01-16T00:00:00.000+01:00"
#         },
#         "expires": "2022-09-30T00:00:00.000+02:00",
#         "cancelled": false,
#         "tuitionFee": {
#             "total": [
#                 16875
#             ],
#             "content": true,
#             "first": [
#                 16875
#             ]
#         },
#         "itdistance": false,
#         "formOfFunding": "ORD",
#         "admissionDetails": {
#             "selectionModel": [
#                 "364UM"
#             ],
#             "distributionOfCredits": [
#                 {
#                     "credit": [
#                         {
#                             "startWeek": [
#                                 202145
#                             ],
#                             "stopWeek": [
#                                 202202
#                             ],
#                             "year": [
#                                 2021
#                             ],
#                             "semester": [
#                                 "ht"
#                             ],
#                             "content": 7.5
#                         }
#                     ]
#                 }
#             ],
#             "admissionRoundId": [
#                 "HT2021"
#             ],
#             "credentialRatingModel": [
#                 "364MM"
#             ]
#         },
#         "uniqueIdentifier": "e14c440d-71d0-11eb-95d4-24a7b2db4a00",
#         "applicationDetails": {
#             "closeDate": "2021-04-15",
#             "applicationModel": "FPMODELL",
#             "visibleToInternationalApplicants": false,
#             "onlyAsPartOfPackage": false,
#             "visibleToSwedishApplicants": true,
#             "onlyAsPartOfProgram": false
#         },
#         "id": "uh_extension_v1",
#         "type": "UHEventExtension",
#         "department": "UKK",
#         "education": "i.uoh.mdh.mma501.21171.20212",
#         "lastEdited": "2021-03-18T14:47:37.186+01:00",
#         "paceOfStudyPercentage": 50,
#         "urls": [
#             {
#                 "lang": "swe",
#                 "content": "http://www.mdh.se/utbildning/kurser?kod=MMA501&l=sv"
#             },
#             {
#                 "lang": "eng",
#                 "content": "http://www.mdh.se/utbildning/kurser?kod=MMA501&l=en"
#             }
#         ],
#         "application": {
#             "code": 21171,
#             "last": "2021-04-15T00:00:00.000+02:00"
#         },
#         "languageOfInstruction": "eng",
#         "provider": "p.uoh.mdh",
#         "timeOfStudy": {
#             "code": "dag",
#             "type": "C_TimeOfStudy"
#         },
#         "location": {
#             "country": "SE",
#             "municipalityCode": 1980
#         }
#     },
#     "provider": {
#         "responsibleBody": "Mälardalens högskola",
#         "type": {
#             "code": "statlig",
#             "type": "C_Body"
#         },
#         "emailAddress": "info@mdh.se",
#         "phone": "021-10 13 00",
#         "name": [
#             {
#                 "lang": "swe",
#                 "content": "Mälardalens högskola"
#             },
#             {
#                 "lang": "eng",
#                 "content": "Mälardalen University"
#             }
#         ],
#         "contactAddress": {
#             "town": "VÄSTERÅS",
#             "streetAddress": "",
#             "postBox": 883,
#             "postCode": "721 23"
#         },
#         "visitAddress": {
#             "town": "VÄSTERÅS",
#             "streetAddress": "Mälardalens högskola",
#             "postCode": "721 23"
#         },
#         "url": {
#             "lang": "swe",
#             "content": "https://www.mdh.se/"
#         }
#     }
# }