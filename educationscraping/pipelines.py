import json
import logging

from itemadapter import ItemAdapter
# from education_opensearch.opensearch_store import OpensearchStore
#from education_elastic_persistence.elastic_store import ElasticStore

class JsonWriterPipeline:

    def open_spider(self, spider):
        self.file = open('scraper_output.jl', 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(ItemAdapter(item).asdict()) + "\n"
        self.file.write(line)
        return item


# class OpenSearchPipeline:
#
#     def __init__(self):
#         self.log = logging.basicConfig(level=logging.INFO)
#         self.log = logging.getLogger(__name__)
#         self.opensearch_store = OpensearchStore()
#
#
#     def open_spider(self, spider):
#         self.log.info('Opening spider and starting new save')
#         self.opensearch_store.start_new_save(spider.name)
#
#
#     def close_spider(self, spider):
#         self.log.info('Close spider')
#         self.opensearch_store.finish_save(spider.name)
#
#
#     def process_item(self, item, spider):
#         item_adapted = ItemAdapter(item).asdict()
#         self.opensearch_store.save_education_to_repository(item_adapted)
#         return item

# To reactivate this pipeline uncomment and add education-elastic-persistence to requirements.txt
# class ElasticSearchPipeline:
#
#     def __init__(self):
#         self.elastic_store = ElasticStore()
#
#
#     def open_spider(self, spider):
#         self.elastic_store.start_new_save(spider.name)
#
#
#     def close_spider(self, spider):
#         self.elastic_store.finish_save(spider.name)
#
#
#     def process_item(self, item, spider):
#         item_adapted = ItemAdapter(item).asdict()
#         self.elastic_store.save_education_to_elastic(item_adapted)
#         return item