# -*- coding: utf-8 -*-
# flake8: noqa

# Scrapy settings for educationscraping project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import os

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'info').upper()

BOT_NAME = 'arbetsformedlingen KLL'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'
SPIDER_MODULES = ['educationscraping.spiders.educations','educationscraping.spiders.educationplans']
NEWSPIDER_MODULE = 'educationscraping.spiders'
REQUEST_FINGERPRINTER_IMPLEMENTATION='2.7'

CRAWLERA_ENABLED = os.getenv('CRAWLERA_ENABLED', 'false').lower() == 'true'
CRAWLERA_URL = 'http://proxy.crawlera.com:8010'
CRAWLERA_APIKEY = os.environ.get('CRAWLERA_KEY', 'no-key-supplied')

DOWNLOADER_MIDDLEWARES = {
    'scrapy_crawlera.CrawleraMiddleware': 300,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,

}

#Only scrape this organization form from SUSA-navet. Intended for debug purpose.
ORGANIZATION_FORM = os.environ.get('ORGANIZATION_FORM', None)

# Limits number of items for each spider before close. Default (i.e. not set) is None and means no limit
CLOSESPIDER_ITEMCOUNT = os.environ.get('CLOSESPIDER_ITEMCOUNT', None)

# Limits number of pages to crawl for each spider before close. Default (i.e. not set) is None and means no limit
CLOSESPIDER_PAGECOUNT =os.environ.get('CLOSESPIDER_PAGECOUNT', None)

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'arbetsformedlingen (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Turn this on as it's useful in knowing the ignored URL dupes during a crawl.
DUPEFILTER_DEBUG = True

AWS_DELIVER_BUCKET = os.environ.get('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY', 'kll')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'abcd1234')
AWS_ENDPOINT_URL = os.environ.get('S3_URL', 'http://localhost:19000')

FEEDS = {
    f's3://{AWS_DELIVER_BUCKET}/%(feed_name_prefix)s_%(name)s_%(start_date_str)s.jsonl': {
        'format': 'jsonlines',
        'encoding': 'utf8'
    }
}

FEED_STORAGES = {
    's3': 'scrapy.extensions.feedexport.S3FeedStorage',
}

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
# COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
    "X-Crawlera-Profile": "desktop",
    "X-Crawlera-Cookies": "disable",
}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'scrapingbot.middlewares.SpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
# DOWNLOADER_MIDDLEWARES = {
#    'scrapingbot.middlewares.DownloaderMiddleware': 543,
# }

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
# ITEM_PIPELINES = {
#    #"arbetsformedlingen.pipelines.JsonWriterPipeline": 300
#    #"arbetsformedlingen.pipelines.ElasticSearchPipeline": 300
#     "educationscraping.pipelines.OpenSearchPipeline": 300
# }

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings  # noqa
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

#Spidermon is a monitoring tool for Scrapy spiders that allows you to write monitors that may validate the execution and the results of your spiders.
#https://spidermon.readthedocs.io/en/latest/getting-started.html
# SPIDERMON_ENABLED = os.getenv('SPIDERMON_ENABLED', 'false').lower() == 'true'

#Whether to drop items that contain validation errors.
#https://spidermon.readthedocs.io/en/latest/item-validation.html
# SPIDERMON_VALIDATION_DROP_ITEMS_WITH_ERRORS = os.getenv('SPIDERMON_VALIDATION_DROP_ITEMS_WITH_ERRORS', 'false').lower() == 'true'
#A dict containing the extensions enabled in your project, and their orders.
#https://doc.scrapy.org/en/latest/topics/settings.html
EXTENSIONS = {
    'spidermon.contrib.scrapy.extensions.Spidermon': 500,
}

#A dict containing the item pipelines to use, and their orders. Order values are arbitrary, but it is customary to define them in the 0-1000 range. Lower orders process before higher orders.
#https://doc.scrapy.org/en/latest/topics/settings.html
# ITEM_PIPELINES = {
#     "spidermon.contrib.scrapy.pipelines.ItemValidationPipeline": 800,
# }

# SPIDERMON_VALIDATION_MODELS = {
#     JobPosting: 'arbetsformedlingen.validators.JobPostingValidation',
#     ExtendedJobPosting: 'arbetsformedlingen.validators.ExtendedJobPostingValidation',
# }

# SPIDERMON_SPIDER_CLOSE_MONITORS = (
#     'arbetsformedlingen.monitors.SpiderCloseMonitorSuite',
#
# )

def parse_env_var_list(inputstr):
    if not inputstr:
        return []
    else:
        return inputstr.split(';')

def parse_env_var_notification_enabled(inputstr):
    off_value = True
    on_value = False

    if inputstr and inputstr.lower() == 'true':
        return_value = on_value
    else:
        return_value = off_value
    return return_value

# If educations should not be mapped into items structure.
SKIP_ITEMS_FORMAT = os.getenv('SKIP_ITEMS_FORMAT', 'false').lower() == 'true'

