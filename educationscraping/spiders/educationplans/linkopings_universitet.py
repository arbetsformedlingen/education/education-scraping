import datetime
import json
import logging
from urllib.parse import urlparse

import jmespath
import scrapy
from scrapy import Request

from educationscraping.html_utils import clean_html

log = logging.getLogger(__name__)

class LinkopingsUniversitetSpider(scrapy.Spider):

    custom_settings = {
        'DOWNLOAD_DELAY': 0.25    # 250 ms of delay
    }

    allowed_domains = ["liu.se"]

    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"
    name = "linkopingsuniversitet"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    START_URL = 'https://liu.se/utbildning'

    courses = {}

    programs = {}

    def start_requests(self):
        yield Request(
                url=self.START_URL,
                callback=self.parse_start_page
            )

    def parse_start_page(self, response):
        # Crawl all programs and their courses
        search_id = '%7BB2723A6C-7D49-4911-B770-64BC3BAA93C6%7D'
        search_programs_template_id = '57019585ade34344a9a1f76fa85211f0'
        start_url_search_programs = f'https://liu.se/sv-se/api/taxonomy/search/search?areas=&term=&searchId={search_id}&filter_templates_sm={search_programs_template_id}&templates_sm={search_programs_template_id}&recruitmentpagesemester_sm=&recruitmentpageeducationlevel_sm=&recruitmentpageplaceofstudy_sm=&recruitmentpagespeed_sm=&recruitmentpageteachingform_sm=&bildaoccurrenceopenforapplication_b=&templateId={search_programs_template_id}&selectedTag=&take=400&sort=asc&view=list&expanded=true'

        yield Request(
            url=start_url_search_programs,
            callback=self.parse_programs_search_result
        )

        areas_values = self.get_areas_values(response)
        semester_values = self.get_semester_values(response)
        search_courses_template_id = '6a51430138ec4196b63e466068d25dd1'
        # Crawl all independent courses (some of them belongs to a program as well).
        # Try to minimize the size of result to less than 200 items, with narrowing search parameters,
        # since there is a max 200 number of resultrows.
        for area_value in areas_values:
            for semester_value in semester_values:
                search_course_url = f'https://liu.se/sv-se/api/taxonomy/search/search?areas={area_value}&term=&searchId={search_id}&filter_templates_sm={search_courses_template_id}&templates_sm={search_courses_template_id}&recruitmentpagesemester_sm={semester_value}&recruitmentpageeducationlevel_sm=&recruitmentpageplaceofstudy_sm=&recruitmentpagespeed_sm=&recruitmentpageteachingform_sm=&bildaoccurrenceopenforapplication_b=&templateId={search_courses_template_id}&filter_recruitmentpagesemester_sm={semester_value}&selectedTag=281c17d838499f5bf83eae14199a73fa&take=400&sort=asc&view=list&expanded=true'

                log.debug(f'Calling url {search_course_url}')
                yield Request(
                    url=search_course_url,
                    callback=self.parse_independent_courses_search_result
                )

    def parse_intro_independent_course(self, response):
        course_title = response.css('.article__title::text').get()
        course_plan_url = response.css('a.link-to-study-guide__link::attr(href)').get()
        url_obj = urlparse(course_plan_url)

        course_code = url_obj.path.rsplit('/', 1)[-1]

        log.debug(f'Parsing intropage independent course, course_code: {course_code}, course_title: {course_title}')

        if not course_code in self.courses:
            log.debug(f'Calling url for independent course: {course_plan_url}')
            yield Request(
                url=course_plan_url,
                callback=self.parse_independent_course,
                meta={'course_code': course_code}
            )
        else:
            log.debug(f'Found independent course_code {course_code} in cache, skipping.')

    def parse_independent_course(self, response):
        course_code = response.meta.get('course_code')
        course_title = response.css('header h1::text').get()
        log.debug(f'Parsing course_code {course_code} - Title: {course_title}')
        course_url = response.url

        column = response.css('section.syllabus.f-2col')
        h2s = column.css("h2::text").getall()
        column_texts = column.css("::text").getall()

        description_texts = []
        learning_objectives = self.extract_text_at_headline('Lärandemål', h2s, column_texts, course_url)
        if learning_objectives:
            description_texts.append(learning_objectives)
        course_content = self.extract_text_at_headline('Kursinnehåll', h2s, column_texts, course_url)
        if course_content:
            description_texts.append(course_content)

        description = '\n'.join(description_texts)
        course = {'course_code': course_code,
                  'course_title': course_title,
                  'course_url': course_url,
                  'description': description}

        self.courses[course_code] = course

        yield course


    def parse_program_course(self, response):

        program_code = response.meta.get("program_code")
        course_code = response.meta.get("course_code")

        course_title = response.css('header h1::text').get()
        log.debug(f'Parsing course_code {course_code} - Title: {course_title}')
        course_url = response.url
        courses_size = response.meta.get("courses_size")

        column = response.css('section.syllabus.f-2col')
        h2s = column.css("h2::text").getall()
        column_texts = column.css("::text").getall()

        description_texts = []
        learning_objectives = self.extract_text_at_headline('Lärandemål', h2s, column_texts, course_url)
        if learning_objectives:
            description_texts.append(learning_objectives)
        course_content = self.extract_text_at_headline('Kursinnehåll', h2s, column_texts, course_url)
        if course_content:
            description_texts.append(course_content)

        description = '\n'.join(description_texts)

        course = {'course_code': course_code,
                    'course_title': course_title,
                    'course_url': course_url,
                    'description': description}
        self.programs[program_code]['courses'].append(course)
        self.courses[course_code] = course

        if len(self.programs[program_code]['courses']) == courses_size:
            log.debug(f'Returning complete program with program_code: {program_code}')
            yield {'program_code': program_code,
                   'program_title': response.meta.get('program_title'),
                   'program_url': response.meta.get('program_url'),
                   'description': response.meta.get('description'),
                   'courses': self.programs[program_code]['courses']}



    def extract_text_at_headline(self, wanted_headline, h2s, column_texts, course_url):
        textbody = ''
        try:
            h2_start_index = column_texts.index(wanted_headline)
            headline_to_break_at = h2s[h2s.index(wanted_headline) + 1]
            h2_stop_index = column_texts.index(headline_to_break_at)
            textbody = '\n'.join(column_texts[h2_start_index:h2_stop_index])
            if textbody:
                textbody = clean_html(textbody)
        except ValueError:
            log.debug(f'wanted_headline {wanted_headline} could not be found for {course_url}')
        return textbody


    def navigate_programplan_courselist(self, response):
        # response.url for example: https://studieinfo.liu.se/program/f7mae/4833
        paragraphs = response.css('#syllabus .aside-block p')
        program_url = response.url
        program_code = None
        for paragraph in paragraphs:
            textlabel = paragraph.css('span.text-label::text').get()
            if 'programkod' in textlabel.lower():
                paragraph_nodes = paragraph.css('::text').getall()
                program_code = clean_html(paragraph_nodes[1])
                log.debug(f'Found programcode {program_code} in program plan {response.url}')

                if program_code and not program_code in self.programs:
                    self.programs[program_code] = {}
                    self.programs[program_code]['courses'] = []

        if not program_code:
            log.error(f'program_code could not be found for url: {program_url}')


        program_description = self.parse_program_description(program_url, response)

        description = response.meta.get('description')
        if not description:
            description = program_description
        elif description and program_description:
            description  = description + '\n' + program_description


        tablerows = response.css('.programplan tr')

        course_urls = []
        for tr in tablerows:
            course_url = tr.css('td a::attr(href)').get()

            if course_url:
                td_course_code = clean_html(tr.css('td::text').get())
                course_urls.append({'course_url': course_url, 'course_code': td_course_code})

        if not course_urls:
            log.error(f'Could not find any course_urls on {response.url}')
            yield {'program_code': program_code,
                   'program_title': response.meta.get('program_title'),
                   'program_url': response.meta.get('program_url'),
                   'description': description,
                   'courses': self.programs[program_code]['courses']}

        courses_size = len(course_urls)
        for url_item in course_urls:

            course_code = url_item['course_code']

            if course_code in self.courses:
                log.debug(f'Found course_code {course_code} in cache')
                self.programs[program_code]['courses'].append(self.courses[course_code])
                if len(self.programs[program_code]['courses']) == courses_size:
                    log.debug(f'Returning complete program with program_code: {program_code}')
                    yield {'program_code': program_code,
                           'program_title': response.meta.get('program_title'),
                           'program_url': response.meta.get('program_url'),
                           'description': description,
                           'courses': self.programs[program_code]['courses']}

            else:
                yield response.follow(
                    url=url_item['course_url'],
                    callback=self.parse_program_course,
                    meta={'program_code': program_code,
                          'program_title': response.meta.get('program_title'),
                          'program_url': response.meta.get('program_url'),
                          'description': description,
                          'course_code': course_code,
                          'courses_size': courses_size}
                )


    def parse_program_description(self, program_url, response):
        column = response.css('section.syllabus.f-2col')
        h2s = column.css("h2::text").getall()
        column_texts = column.css("::text").getall()
        description_texts = []
        program_introduction = self.extract_text_at_headline('Inledning', h2s, column_texts, program_url)
        if program_introduction:
            description_texts.append(program_introduction)
        program_goal = self.extract_text_at_headline('Mål', h2s, column_texts, program_url)
        if program_goal:
            description_texts.append(program_goal)
        program_content = self.extract_text_at_headline('Innehåll', h2s, column_texts, program_url)
        if program_content:
            description_texts.append(program_content)
        return '\n'.join(description_texts)

    def parse_program_page(self, response):
        # response.url for example: 'https://liu.se/utbildning/program/mgba2'
        program_title = clean_html(response.css('.article__title').get())

        program_article_text = '\n'.join([clean_html(textline) for textline in response.css('.article .article__text *::text').getall()]).strip()
        description = program_article_text

        program_url = response.url

        a_tag_to_follow = response.css('a.link-to-study-guide__link::attr(href)').get()

        if not a_tag_to_follow:
            # Some pages don't have link-to-study-guide__link,
            # try to get the link in another place.
            a_tag_complete = response.css('.program-table a.to::attr(href)').get()
            # Remove last part of url since the complete url suddenly lead to http 404/not found.
            # https://studieinfo.liu.se/program/6cyyy/1396895 -> https://studieinfo.liu.se/program/6cyyy
            if a_tag_complete:
                parts = a_tag_complete.split('/')
                a_tag_to_follow = '/'.join(parts[:-1])

        if a_tag_to_follow:
            yield Request(
                url=a_tag_to_follow,
                callback=self.navigate_programplan_courselist,
                meta={'program_url': program_url, 'program_title': program_title, 'description': description}
            )
        else:
            log.error(f'Found no href to programplan in page {program_url}')

    def parse_programs_search_result(self, response):

        data = json.loads(response.body)

        urls = jmespath.search('Hits[*].Url', data)
        unique_urls = set(urls)

        log.debug(f'Found {len(unique_urls)} unique urls in the search result for programs')

        for unique_url in unique_urls:
            yield Request(
                url=unique_url,
                callback=self.parse_program_page
            )

    def parse_independent_courses_search_result(self, response):

        data = json.loads(response.body)

        urls = jmespath.search('Hits[*].Url', data)
        unique_urls = set(urls)

        log.debug(f'Found {len(unique_urls)} unique urls in the search result for courses')

        for unique_url in unique_urls:
            yield Request(
                url=unique_url,
                callback=self.parse_intro_independent_course
            )



    def get_semester_values(self, response):
        btn_semesters = response.css("button[name='filter_recruitmentpagesemester_sm']")
        semester_values = []
        for btn_semester in btn_semesters:
            btn_value = btn_semester.attrib['value']
            semester_values.append(btn_value)
        return semester_values


    def get_areas_values(self, response):
        areas_options = response.css("select[name='areas'] option")
        areas_values = []
        for areas_option in areas_options:
            areas_value = areas_option.attrib['value']
            areas_values.append(areas_value)
        log.debug(f"Found {len(areas_values)} areas")
        return areas_values

