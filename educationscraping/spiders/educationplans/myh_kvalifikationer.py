import datetime
import json
import logging
import re

import jmespath
import scrapy
from scrapy import Request
from scrapy.utils.project import get_project_settings

log = logging.getLogger(__name__)


class MyhKvalifikationerSpider(scrapy.Spider):
    settings = get_project_settings()
    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"
    name = "myhkvalifikationer"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    # Currently the API seems to choke when responses get too large (> 32KiB), hence 90 as limit.
    current_page = 1
    QUALIFICATIONS_PAGINATION_URL = "https://qdb-external-api.myh.se/api/v1/Kvalifikationer?limit=90"
    QUALIFICATION_DETAIL_URL = "https://qdb-external-api.myh.se/api/v1/Kvalifikationer/"

    def start_requests(self):

        yield Request(
            url=self.get_qualifications_list_url(),
            callback=self.parse_qualifications_list
        )

    def get_qualifications_list_url(self):
        return self.QUALIFICATIONS_PAGINATION_URL + f"&page={self.current_page}"

    def parse_qualifications_list(self, response):
        json_response = json.loads(response.body)

        qualifications_list = jmespath.search('kvalifikationer', json_response)

        for qualifications_list_item in qualifications_list:
            qualification_id = qualifications_list_item['id']
            # For example: https://qdb-external-api.myh.se/api/v1/Kvalifikationer/a3ce901c-730e-4fd9-9929-20c5bd60ff20?include_archived=false
            detail_url = self.QUALIFICATION_DETAIL_URL + f'{qualification_id}?include_archived=false'
            log.debug(f'Calling detail url {detail_url}')
            yield Request(
                url=detail_url,
                callback=self.parse_qualification_detail
            )

        if json_response['page'] < json_response['total_pages']:
            self.current_page += 1
            url = self.get_qualifications_list_url()
            log.info(f'Calling pagination url {url}')
            yield Request(
                url=url,
                callback=self.parse_qualifications_list
            )

    RE_VALID_ALTERNATIVE_ID_SHORT = re.compile(r"(YH\d+$)", re.UNICODE)
    RE_VALID_ALTERNATIVE_ID = re.compile(r"(YH\d+-\d{4}$)", re.UNICODE)

    def convert_alternative_id(self, alternative_id):
        '''
        :param alternative_id: For example 'YH00784-2019'
        :return:
        'YH00784-2019' -> 'YH00784'
        'YH00784' -> 'YH00784'
        'ABC00784-2019' -> None
        '''
        if self.RE_VALID_ALTERNATIVE_ID_SHORT.findall(alternative_id):
            converted_id = alternative_id
        elif self.RE_VALID_ALTERNATIVE_ID.findall(alternative_id):
            converted_id = re.sub("-\d{4}$", "", alternative_id)
        else:
            converted_id = None
        return converted_id

    def parse_qualification_detail(self, response):
        json_response = json.loads(response.body)

        qualification = {}

        alternative_id = jmespath.search('alternativt_id', json_response)
        converted_alternative_id = self.convert_alternative_id(alternative_id)
        qualification['program_code'] = converted_alternative_id

        qualification['title'] = jmespath.search('namn', json_response)

        learning_results = []
        learning_results.append(jmespath.search('resultat_av_larande_kunskaper', json_response))
        learning_results.append(jmespath.search('resultat_av_larande_fardigheter', json_response))
        learning_results.append(jmespath.search('resultat_av_larande_kompetenser', json_response))

        description = '\n'.join(learning_results)
        qualification['description'] = description

        qualification['title'] = jmespath.search('namn', json_response)

        yield qualification

