import datetime
import logging
from urllib import parse
import scrapy
from scrapy import Selector
from scrapy.utils.project import get_project_settings
from educationscraping.html_utils import clean_html

log = logging.getLogger(__name__)

settings = get_project_settings()

class SkolverketSpider(scrapy.Spider):

    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"

    name = "skolverket"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    # The number of courses to be fetched. Set to None to fetch all. Otherwise set to a number.
    # Same idea as scrapy CLOSESPIDER_ITEMCOUNT, which doesn't work for
    # this spider because of the solution pattern:
    number_of_courses_limit = None

    courses = {}

    def start_requests(self):
        courses_url = "https://www.skolverket.se/undervisning/gymnasieskolan/laroplan-program-och-amnen-i-gymnasieskolan/hitta-program-amnen-och-kurser-i-gymnasieskolan?url=907561864%2Fsyllabuscw%2Fjsp%2Fsearch.htm%3FalphaSearchString%3D%26searchType%3DFREETEXT%26searchRange%3DCOURSE%26subjectCategory%3D%26searchString%3D&sv.url=12.5dfee44715d35a5cdfa8e7a"
        yield scrapy.http.Request(courses_url, callback=self.parse_courses)

    #Parse all course infos at Skolverket:
    def parse_courses(self, response):
        course_links = response.css("table.searchresult th[scope=row] a").getall()
        all_courses_urls = []
        for course_link in course_links:
            relative_url = Selector(text=course_link).css("a::attr(href)").get()
            link_url = 'https://www.skolverket.se' + str(relative_url)
            link_text = Selector(text=course_link).css("a::text").get()
            all_courses_urls.append({'link_url': link_url, 'link_text': link_text})

        yield from self.start_course_scraping(all_courses_urls)

    def start_course_scraping(self, all_courses_urls):
        if len(all_courses_urls) > 0:
            # Fetch next course url from list and remove it from remaining:
            link = all_courses_urls.pop(0)
            absolute_url = link.get('link_url')
            link_text = link.get('link_text')
            course_code = None
            try:
                course_code=parse.parse_qs(parse.urlparse(absolute_url).query)['courseCode'][0]
            except Exception as e:
                log.error(f'Failed to parse course_code for url {link}')
            kwargs = {}
            kwargs['remaining_urls'] = all_courses_urls
            kwargs['course_code'] = course_code
            kwargs['link_text'] = link_text
            yield scrapy.http.Request(absolute_url, cb_kwargs=kwargs, callback=self.parse_course)
        else:
            # No courses was found, continue by scraping programs...
            yield from self.start_programs_scraping()

    def parse_course(self, response, **kwargs):
        remaining_urls = kwargs['remaining_urls']
        course_code = kwargs['course_code']
        link_text = kwargs['link_text']

        all_course_sections = response.css("div.course-details").getall()

        # Find the course on the page that matches the anchor link:
        for course_section in all_course_sections:
            course_code_tag = Selector(text=course_section).css("span.code").get()
            if str(course_code) in str(course_code_tag):
                log.debug(f"Match! {course_code} vs {course_code_tag}")
                course = {}
                course['title'] = link_text
                course['description'] = clean_html(course_section)
                course['course_code'] = course_code
                self.courses[course_code] = course
            else:
                log.debug(f"No match! {course_code} vs {course_code_tag}")
        yield from self.next_course(remaining_urls)

    def next_course(self, remaining_urls):
        if len(remaining_urls) > 0 and (not self.number_of_courses_limit or len(self.courses) <= self.number_of_courses_limit):
            link = remaining_urls.pop(0)
            absolute_url = link.get('link_url')
            link_text = link.get('link_text')
            course_code = None
            try:
                course_code=parse.parse_qs(parse.urlparse(absolute_url).query)['courseCode'][0]
            except Exception as e:
                log.error(f'Failed to parse course_code for url {absolute_url}')
            kwargs = {}
            kwargs['remaining_urls'] = remaining_urls
            kwargs['course_code'] = course_code
            kwargs['link_text'] = link_text
            yield scrapy.http.Request(absolute_url, cb_kwargs=kwargs, callback=self.parse_course)
        else:
            # No more courses to scrape. Continue by scraping programs...
            yield from self.start_programs_scraping()

    def start_programs_scraping(self):
        url = 'https://www.skolverket.se/undervisning/gymnasieskolan/laroplan-program-och-amnen-i-gymnasieskolan/gymnasieprogrammen'
        yield scrapy.http.Request(url, callback=self.parse_all_programs)

    def parse_all_programs(self, response):
        all_educations_urls = response.css("p.sv-font-oversiktslankar a::attr(href)").getall()
        for education_url in all_educations_urls:
            program_code = None
            try:
                program_code=parse.parse_qs(parse.urlparse(education_url).query)['programCode'][0]
            except Exception as e:
                log.error(f'Failed to parse program_code for url {education_url}')
            yield response.follow(education_url, callback=self.parse, meta={"program_code": program_code})

    def parse(self, response):
        program_code = response.meta["program_code"]
        program_description = clean_html(response.css("div #Examensmål").get())
        title = clean_html(response.css("h1").get())
        all_courses_urls = response.css("table.searchresult th[scope=row] a::attr(href)").getall()
        course_infos = []
        for course_url in all_courses_urls:
            course_code = None
            try:
                course_code=parse.parse_qs(parse.urlparse(course_url).query)['courseCode'][0]
            except Exception as e:
                log.error(f'Failed to parse course_code for url {course_code}')
            if course_code:
                course_info = self.courses.get(course_code)
                if course_info:
                    # course_dict = {'course_code': course_code, 'description': course_info}
                    course_infos.append(course_info)
        if program_description:
            yield {'program_code': program_code, 'title': title, 'description': program_description, 'courses': course_infos}
        else:
            log.warning(f"No exam goals found. Courses: {course_infos}")
