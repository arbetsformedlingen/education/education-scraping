import datetime
import json
import logging
import re

import scrapy
from scrapy import Request
from scrapy import Selector
from scrapy.utils.project import get_project_settings
from educationscraping.html_utils import clean_html

log = logging.getLogger(__name__)


class StockholmsUniversitetSpider(scrapy.Spider):
    settings = get_project_settings()
    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"
    name = "stockholmsuniversitet"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    # The number of items (courses and programs) to be fetched. Set to None to fetch all.
    # Otherwise set to a number.
    # Same idea as scrapy CLOSESPIDER_ITEMCOUNT, which doesn't work for
    # this spider because of the solution pattern:
    number_of_items_limit = int(settings['CLOSESPIDER_ITEMCOUNT']) if settings['CLOSESPIDER_ITEMCOUNT'] else None

    courses = {}
    course_urls = []

    programs = {}
    program_urls = []

    COURSES_PAGINATION_URL = "https://www.su.se/edusearch?notforcedreason=0&edutype=1&q=&langsearchresults=sv&count=100&page={page}"
    PROGRAM_PAGINATION_URL = "https://www.su.se/edusearch?notforcedreason=0&edutype=2&q=&langsearchresults=sv&count=100&page={page}"

    def start_requests(self):
        yield Request(
            url=self.COURSES_PAGINATION_URL.format(page=1),
            callback=self.parse_courses_page,
            cb_kwargs={"page": 1},
        )

    def parse_courses_page(self, response, page=None):
        data = json.loads(response.body)

        # Response example:
        # {"totalRows":2333,"responseTime":32,"queryId":"1f50829028aa4b85b8285a031c0a4a16","fixedQuery":"",
        # "usingPostFilters":false,"noOfPostFilterRemoved":0,"noOfResultRows":0,
        # "query":{"query":"*","page":100,"perPage":100,"isEnglishSearch":false,"postFilterMode":"basic"},"resultRows":[]}
        max_items_scraped = False
        if self.number_of_items_limit:
            if len(self.courses) >= self.number_of_items_limit:
                max_items_scraped = True
        if data.get('resultRows') and len(data.get('resultRows')) > 0 and not max_items_scraped:
            for course in data.get('resultRows'):
                course.pop('events', None)
                # Add course to cache...
                course_code = course.get('educationCode').upper()
                course_dict = {'meta': course, 'code': course_code}
                self.courses[course_code] = course_dict
                self.course_urls.append(course.get('url'))
            yield from self.paginate_courses_next_page(page=page + 1)
        else:
            yield from self.start_course_scraping()

    def paginate_courses_next_page(self, page):
        yield Request(
            url=self.COURSES_PAGINATION_URL.format(page=page),
            callback=self.parse_courses_page,
            cb_kwargs={"page": page},
        )

    def start_course_scraping(self):
        if len(self.course_urls) > 0:
            url = self.course_urls.pop(0)
            yield scrapy.http.Request(url, callback=self.parse_course)
        else:
            yield from self.start_program_scraping()

    def parse_course(self, response):

        # Response example tag course_code:
        # <span class="mb-0 text-sans-serif-bold text-uppercase">PEG102</span>
        course_code = clean_html(response.css("span.mb-0.text-sans-serif-bold.text-uppercase").get())

        if not course_code:
            log.warning(f'No course code found on url {response.url}. Skipping.')
            yield from self.next_course()
            return

        course_title = clean_html(response.css("h1#education-name").get())

        # Response example tag course article:
        # <article class="webb2021-article col-lg-8 col-sm-12 main-column-padding-right mb-4 main-column-left">
        course_article = response.css("article.webb2021-article.col-lg-8.col-sm-12.main-column-padding-right.mb-4.main-column-left").get()
        course_article_tag = Selector(text=course_article)

        # Response example tag education description:
        # <div id="education-description">
        course_description = clean_html(course_article_tag.css("div#education-description").get())

        # Add course details to the course...
        course = self.courses.get(course_code)
        course['title'] = course_title
        course['description'] = course_description
        self.courses[course_code] = course

        yield from self.next_course()

    def next_course(self):
        if len(self.course_urls) > 0:
            url = self.course_urls.pop(0)
            yield scrapy.http.Request(url, callback=self.parse_course)
        else:
            yield from self.paginate_programs(1)

    def paginate_programs(self, page=1):
        yield Request(
            url=self.PROGRAM_PAGINATION_URL.format(page=page),
            callback=self.parse_programs_page,
            cb_kwargs={"page": page},
        )

    def parse_programs_page(self, response, page=None):
        data = json.loads(response.body)

        # Response example program:
        # {"totalRows":2333,"responseTime":32,"queryId":"1f50829028aa4b85b8285a031c0a4a16","fixedQuery":"",
        # "usingPostFilters":false,"noOfPostFilterRemoved":0,"noOfResultRows":0,
        # "query":{"query":"*","page":100,"perPage":100,"isEnglishSearch":false,"postFilterMode":"basic"},"resultRows":[]}

        max_items_scraped = False
        if self.number_of_items_limit:
            if len(self.programs) >= self.number_of_items_limit:
                max_items_scraped = True
        if data.get('resultRows') and len(data.get('resultRows')) > 0 and not max_items_scraped:
            for program in data.get('resultRows'):
                # Add program to cache...
                program_code = program.get('educationCode')
                self.programs[program_code] = program
                self.program_urls.append(program.get('url'))
            yield from self.paginate_programs(page=page + 1)
        else:
            yield from self.start_program_scraping()

    def start_program_scraping(self):

        yield from self.yield_educations()

        if len(self.program_urls) > 0:
            for url in self.program_urls:
                yield scrapy.http.Request(url, callback=self.parse_program)
        else:
            yield None

    def yield_educations(self):
        # Yield dicts with all cached courses...
        if len(self.courses) > 0:
            for course_code, course in self.courses.items():
                yield {'course_code': course_code, 'title': course.get('title'), 'description': course.get('description')}

    def parse_program(self, response):
        program_article = response.css("article.webb2021-article.col-lg-8.col-sm-12.main-column-padding-right.mb-4.main-column-left").get()
        program_title = clean_html(response.css("h1#education-name").get())
        program_code = clean_html(response.css("span.mb-0.text-sans-serif-bold.text-uppercase").get())

        program_article_tag = Selector(text=program_article)

        '''
        Response example tag program brief
        <p class="lead-light mb-4"></p>
        '''
        program_brief = clean_html(program_article_tag.css("p.lead-light.mb-4").get())

        '''
        Response example tag program description:
        <div id="education-description">
        '''
        program_description = clean_html(program_article_tag.css("div#education-description").get())

        '''
        Response example tag program details:
        <div class="su-js-has-toggle-btn track-collapse-state
        collapse show" id="program-detail" style="">
        '''
        program_details = clean_html(program_article_tag.css("div#program-detail").get())

        '''
        Response example tag labor market for program:
        <div class="su-js-has-toggle-btn track-collapse-state
        collapse show" id="programme-labor-market" style="">
        '''
        program_labor_market = clean_html(program_article_tag.css("div#programme-labor-market").get())

        program_meta = self.programs.get(program_code)

        course_infos_for_program_dict = {}

        # Find course codes in text...
        course_codes_in_program_article = su_extract_course_codes(program_article)

        if course_codes_in_program_article:
            for course_code in course_codes_in_program_article:
                course_info = self.courses.get(course_code)
                if course_info and not course_infos_for_program_dict.get('course_code'):
                    course_dict = {'course_code': course_code, 'course': course_info}
                    course_infos_for_program_dict[course_code] = course_dict

        all_links = program_article_tag.css("a::attr(href)").getall()

        log.debug(f"Program: {program_code}, Number of courses code found in free text: {len(all_links)}, "
                  f"number of courses codes found in program code: {len(course_codes_in_program_article)}")

        # Find course codes in links...
        for link in all_links:
            course_code = su_extract_course_code_from_event_link(link)
            if course_code:
                course_info = self.courses.get(course_code)
                if course_info and not course_infos_for_program_dict.get('course_code'):
                    course_dict = {'course_code': course_code, 'course': course_info}
                    course_infos_for_program_dict[course_code] = course_dict
                else:
                    log.debug(f'Didnt find course info for course {course_code}')

        course_infos_for_program = []
        for code, course in course_infos_for_program_dict.items():
            course_infos_for_program.append(course)

        yield {
            'program_code': program_code,
            'title': program_title,
            'brief': program_brief,
            'description': program_description,
            'details': program_details,
            'labor_market': program_labor_market,
            'meta': program_meta,
            'courses': course_infos_for_program
        }


def su_extract_course_code_from_event_link(link):
    # Examples links that contain course codes:
    # href="/sok-kurser-och-program/ib130n-1.496492">Introduktion till data- och systemvetenskap 7,5 hp</a>
    # <a href="/sok-kurser-och-program/ue2102-1.492790"
    course_code = None
    if link:
        link_str = str(link)
        link_upper = link_str.upper()
        event_match = re.search("[A-Z][A-Z][[A-Z0-9][[A-Z0-9][A-Z0-9][A-Z0-9]-.{8}$", link_upper)
        if event_match:
            event_code = event_match.group()
            if event_code:
                course_code_match = re.search("^.{6}", event_code)
                if course_code_match:
                    course_code = course_code_match.group()
    return course_code


def su_extract_course_codes(text):
    course_codes_in_exam_goals = None
    if text:
        cleaned_text = clean_html(text)
        text_uppercase = cleaned_text.upper()
        course_codes_in_exam_goals = re.findall("([A-Z][A-Z][[A-Z0-9][[A-Z0-9][A-Z0-9][A-Z0-9])", text_uppercase)
    return course_codes_in_exam_goals