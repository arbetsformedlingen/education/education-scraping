import logging
import scrapy
import datetime
from scrapy import Request, Selector
from educationscraping.html_utils import clean_html

log = logging.getLogger(__name__)


class UmeaUniversitetSpider(scrapy.Spider):
    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"
    name = "umeauniversitet"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    COURSES_PAGINATION_URL = "https://www.umu.se/utbildning/sok/?edu=c&order=p&page={page}"
    PROGRAM_PAGINATION_URL = "https://www.umu.se/utbildning/sok/?edu=p&order=c&page={page}"

    courses = {}

    def start_requests(self):
        yield Request(
            url=self.COURSES_PAGINATION_URL.format(page=1),
            callback=self.parse_courses,
            cb_kwargs={"page": 1},
        )

    def parse_courses(self, response, **kwargs):
        all_courses_links = response.css("a.eduName::attr(href)").getall()
        if all_courses_links:
            for course_link_relative in all_courses_links:
                course_link_absolute = response.urljoin(course_link_relative)
                yield scrapy.http.Request(course_link_absolute, callback=self.parse_course)
            page = kwargs['page']
            kwargs['page'] = int(page) + 1
            yield scrapy.http.Request(self.COURSES_PAGINATION_URL.format(page=page), cb_kwargs=kwargs,
                                      callback=self.parse_courses)
        # else start_scrape_programs...
        else:
            yield from self.start_programs_scraping()

    def parse_course(self, response):
        title = clean_html(response.css("h1").get())
        about_the_course = response.css("section.maincontent").get()
        #about_the_course2 = response.css("section.maincontent:not(div.document-links.after-text)").get()
        kwargs = {}
        kwargs['title'] = title
        kwargs['about_the_course'] = about_the_course

        # Fetch last link (e.g. that contains the latest course plan)...
        kursplan_relative_links = response.css('a[href*="kursplan"]::attr(href)').getall()
        if len(kursplan_relative_links) > 0:
            last_kursplan_relative_links = kursplan_relative_links[-1]
            kursplan_absolute_link = response.urljoin(last_kursplan_relative_links)
            yield scrapy.http.Request(kursplan_absolute_link, cb_kwargs=kwargs, callback=self.parse_course_plan)

    def parse_course_plan(self, response, **kwargs):
        kurskod_section = response.css("div.kod").get()
        kurskod_cleaned = clean_html(kurskod_section)
        # Course code is in this format after html clean: 'Kurskod: 5TF067'
        course_code = kurskod_cleaned.partition("Kurskod: ")[2]
        studieplaner = response.css("div.studieplaninnehall").getall()
        kursplan_innehall = ''
        kursplan_studieresultat = ''
        for studieplan in studieplaner:
            studieplan_selector = Selector(text=studieplan)
            if studieplan_selector.css("h2#innehall").get():
                kursplan_innehall = studieplan
            elif studieplan_selector.css("h2#forvantadestudieresultat").get():
                kursplan_studieresultat = studieplan
            # Sections that should not be included...
            elif studieplan_selector.css("h2#behorighetskrav").get() or studieplan_selector.css(
                    "h2#undervisningensupplagg").get() or studieplan_selector.css(
                "h2#examination").get() or studieplan_selector.css("h2#ovrigaforeskrifter").get():
                pass

        description = clean_html(
            f"{kwargs['about_the_course']} {kursplan_innehall} {kursplan_studieresultat}")
        course = {'course_code': course_code, 'title': kwargs['title'], 'description': description}
        # Add course to cache to make it available when scraping education programs...
        self.courses[course_code.upper()] = course

        yield course

    def start_programs_scraping(self):
        yield scrapy.http.Request(url=self.PROGRAM_PAGINATION_URL.format(page=1),
                                  callback=self.parse_programs,
                                  cb_kwargs={"page": 1})

    def parse_programs(self, response, **kwargs):
        all_programs_links = response.css("a.eduName::attr(href)").getall()
        if all_programs_links:
            for programs_link_relative in all_programs_links:
                program_link_absolute = response.urljoin(programs_link_relative)
                yield scrapy.http.Request(program_link_absolute, callback=self.parse_program)
            page = kwargs['page']
            kwargs['page'] = int(page) + 1
            yield scrapy.http.Request(self.PROGRAM_PAGINATION_URL.format(page=page), cb_kwargs=kwargs,
                                      callback=self.parse_programs)

    def parse_program(self, response):
        title = clean_html(response.css("h1").get())
        about_the_program_sections = response.css("div.columns.medium-centered").getall()
        about_the_program_ingress = ''
        labormarket = ''
        for about_the_program_section in about_the_program_sections:
            about_the_program_selector = Selector(text=about_the_program_section)
            if about_the_program_selector.css("ingress").get():
                about_the_program_ingress = about_the_program_section
            elif about_the_program_selector.css("h2#efterutbildningen").get():
                labormarket = about_the_program_section

        kursplan_relative_links = response.css('a[href*="kursplan"]::attr(href)').getall()

        program_courses = []
        for kursplan_link in kursplan_relative_links:
            # Parse course code from link (e.g. '/utbildning/kursplan/5tf080/rev/31080/')...
            course_code_substring = kursplan_link.partition("kursplan/")[2]
            course_code = course_code_substring.partition("/")[0].upper()
            if self.courses.get(course_code):
                log.debug(f"Found course_plan {course_code} for the program {title}")
                course = self.courses.get(course_code)
                program_courses.append(course)

        kwargs = {}
        kwargs['title'] = title
        kwargs['about_the_program_ingress'] = about_the_program_ingress
        kwargs['labormarket'] = labormarket
        kwargs['courses'] = program_courses

        # Fetch last link (e.g. that contains the latest program plan)...
        programplan_relative_links = response.css('a[href*="utbildningsplan"]::attr(href)').getall()
        if len(programplan_relative_links) > 0:
            last_programplan_relative_links = programplan_relative_links[-1]
            programplan_absolute_link = response.urljoin(last_programplan_relative_links)
            yield scrapy.http.Request(programplan_absolute_link, cb_kwargs=kwargs, callback=self.parse_program_plan)

    def parse_program_plan(self, response, **kwargs):
        programkod_section = response.css("div.kod").get()
        programkod_cleaned = clean_html(programkod_section)
        # Programkod is in this format after html clean: 'Programkod: 5TF067'
        program_code = programkod_cleaned.partition("Programkod: ")[2]

        description = clean_html(kwargs['about_the_program_ingress'])
        labormarket = clean_html(kwargs['labormarket'])

        studieplaner = response.css("div.studieplaninnehall").getall()
        kursplan_innehall = ''

        for studieplan in studieplaner:
            studieplan_selector = Selector(text=studieplan)
            # Sections that should not be included...
            if studieplan_selector.css("h2#behorighetskrav").get() or studieplan_selector.css(
                    "h2#undervisningensupplagg").get() or studieplan_selector.css(
                "h2#examination").get() or studieplan_selector.css("h2#ovrigaforeskrifter").get():
                pass
            else:
                # Include unknown sections (i.e. might contain course plan info)...
                kursplan_innehall = kursplan_innehall + " " + studieplan

        details = clean_html(kursplan_innehall)

        yield {'program_code': program_code, 'title': kwargs['title'], 'description':
            description, 'details': details, 'labor_market': labormarket, 'courses': kwargs['courses']}
