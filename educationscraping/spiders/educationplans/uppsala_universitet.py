import logging
import scrapy
import datetime
from scrapy import Request
from scrapy.utils.project import get_project_settings
from urllib.parse import urlparse, parse_qs

import json

log = logging.getLogger(__name__)


class UppsalaUniversitetSpider(scrapy.Spider):

    settings = get_project_settings()
    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educationplans"
    name = "uppsalauniversitet"
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")

    # The number of items (courses and programs) to be fetched. Set to None to fetch all.
    # Otherwise set to a number.
    # Same idea as scrapy CLOSESPIDER_ITEMCOUNT, which doesn't work for
    # this spider because of the solution pattern:
    # Limit number of educations to fetch if number_of_items_limit...
    number_of_items_limit = int(settings['CLOSESPIDER_ITEMCOUNT']) if settings['CLOSESPIDER_ITEMCOUNT'] else None

    BASE_URL = "https://www.uu.se"
    COURSES_PAGINATION_URL = "https://www.uu.se/appresource/4.37c917b0182e93101d11d40d/12.2aeab24c18b37dfecdbd75c/search"
    PROGRAM_PAGINATION_URL = "https://www.uu.se/appresource/4.37c917b0182e93101d11d40d/12.2aeab24c18b37dfecdbd75c/search"

    courses = {}
    course_urls = []

    programs = {}
    program_urls = []

    headers = {
        'Content-Type': 'application/json'
    }
    payload = {
        'category': 'coursesAndProgrammes',
        'query': '',
        'start': 0,
        'type': "Kurs",
        'showMore': 'true'
    }

    def start_requests(self):
        yield Request(
            url=self.COURSES_PAGINATION_URL,
            method='POST',
            headers=self.headers,
            body=json.dumps(self.payload),
            callback=self.parse_course_pages,
            cb_kwargs={"start": 0},
        )

    def paginate_courses_next_page(self, start):
        '''
        Send requests to paginate and fetch the next page
        of course data.
        '''
        self.payload['start'] = start
        yield Request(
            url=self.COURSES_PAGINATION_URL,
            method='POST',
            headers=self.headers,
            body=json.dumps(self.payload),
            callback=self.parse_course_pages,
            cb_kwargs={"start": start},
        )

    def parse_course_pages(self, response, start=None):
        '''
        Sample response data
        {
        "date": null,
        "image": null,
        "description": "I den här kursen jämförs olika lokala perspektiv på den förhistoriska utvecklingen i Östersjöområdet för att identifiera kulturella likheter och skillnader; likheter och skillnader baserade, inte på&nbsp;&hellip;",
        "details": [
            "Kurs, Hösten 2023, Uppsala, Campus, 50&nbsp;%, Engelska",
            "Kurs, Hösten 2023, Visby, Campus, 50&nbsp;%, Engelska"
        ],
        "title": "Östersjöområdets förhistoria, 7,5&nbsp;hp (5AR770)",
        "uri": "/utbildning/kurs?query=5AR770"
        }
        '''
        data = json.loads(response.body)
        results = data.get('result', {}).get('hits', [])

        # Checking if the number of scraped items has reached the limit.
        max_items_scraped = False
        if self.number_of_items_limit:
            if len(self.courses) >= self.number_of_items_limit:
                max_items_scraped = True

        if results and len(results) > 0 and not max_items_scraped:
            for item in results:
                course_code = item.get('uri').split('=')[-1]
                course_url = self.BASE_URL + (item.get('uri') or '')
                # Parse the URL
                parsed_url = urlparse(course_url)

                # Get the query parameters as a dictionary
                query_params = parse_qs(parsed_url.query)

                # Check if the 'query' parameter exists
                if 'query' in query_params:
                    # Create a dictionary for the course
                    course = {
                        'url': course_url
                    }
                    # Append the course dictionary to the list in the directory
                    self.courses[course_code] = course
                '''
                Increasing the start parameter by 10 for pagination,
                as each page contains 10 records.
                '''
            yield from self.paginate_courses_next_page(start=start + 10)
        else:
            yield from self.start_course_scraping()

    def start_course_scraping(self):
        if len(self.courses) > 0:
            for course_code, course in self.courses.items():
                course_url = course['url']
                kwargs = {}
                kwargs['course_code'] = course_code
                yield scrapy.http.Request(course_url, cb_kwargs=kwargs, callback=self.parse_course)
        else:
            yield from self.paginate_programs(0)

    def parse_course(self, response, **kwargs):
        course_code = kwargs['course_code']
        title = response.css("div.education-header > h1.education-header-name::text").get().strip()
        description_section = response.css("div.education > p::text").getall()
        description_text = ''.join(description_section)

        yield {'course_code': course_code, 'title': title, 'description': description_text}

    def paginate_programs(self, start):
        '''
        Send requests to paginate and fetch the program data.
        '''
        self.payload['type'] = 'Program'
        self.payload['start'] = start
        yield Request(
            url=self.PROGRAM_PAGINATION_URL,
            method='POST',
            headers=self.headers,
            body=json.dumps(self.payload),
            callback=self.parse_program_pages,
            cb_kwargs={"start": start},
        )

    def parse_program_pages(self, response, start=0):
        '''
        Sample response data
        {
        "date": null,
        "image": "<img src="/images/200.5afc0f1118c80b470232d63/1703067468154/Civilingenj%C3%B6rsprogrammet-i-industriell-ekonomi.jpg\" srcset=\"/images/200.5afc0f1118c80b470232d63/1703067468154/x160p/Civilingenj%C3%B6rsprogrammet-i-industriell-ekonomi.jpg 160w, /images/200.5afc0f1118c80b470232d63/1703067468154/x320p/Civilingenj%C3%B6rsprogrammet-i-industriell-ekonomi.jpg 320w, /images/200.5afc0f1118c80b470232d63/1703067468154/x480p/Civilingenj%C3%B6rsprogrammet-i-industriell-ekonomi.jpg 480w, /images/200.5afc0f1118c80b470232d63/1703067468154/Civilingenj%C3%B6rsprogrammet-i-industriell-ekonomi.jpg 500w\" sizes=\"100vw\" class=\"sv-noborder\" alt=\"\"/>",
        "description": "Vill du vara med och skapa framtidens industri, med intelligenta maskiner, hållbara produkter och tjänster, och effektiva lösningar? Vill du utveckla nya innovativa koncept, ha världen som&nbsp;&hellip;",
        "details": [
            "Program, Hösten 2023, Uppsala, Campus, 100&nbsp;%, Svenska",
            "Program, Hösten 2024, Uppsala, Campus, 100&nbsp;%, Svenska"
        ],
        "title": "Civilingenjörsprogrammet i industriell ekonomi, 300&nbsp;hp (TIE2Y)",
        "uri": "/utbildning/program/civilingenjorsprogrammet-industriell-ekonomi"
        }
        '''
        data = json.loads(response.body)
        results = data.get('result', {}).get('hits', [])

        # Checking if the number of scraped items has reached the limit.
        max_items_scraped = False
        if self.number_of_items_limit:
            if len(self.programs) >= self.number_of_items_limit:
                max_items_scraped = True

        if results and len(results) > 0 and not max_items_scraped:
            for item in results:
                program_code = item.get('title').split('(')[-1].split(')')[0]
                program_url = self.BASE_URL + (item.get('uri') or '')
                # Create a dictionary for the program
                program = {
                    'url': program_url
                }
                # Append the program dictionary to the list in the directory
                self.programs[program_code] = program
                '''
                Increasing the start parameter by 10 for pagination,
                as each page contains 10 records.
                '''
            yield from self.paginate_programs(start=start + 10)
        else:
            yield from self.start_program_scraping()

    def start_program_scraping(self):
        '''
        Start scraping for programs.

        If there are programs to scrape, iterates through each program,
        extracts its URL, and initiates a request to parse the program's details.
        '''
        if len(self.programs) > 0:
            for program_code, program in self.programs.items():
                program_url = program['url']
                kwargs = {}
                kwargs['program_code'] = program_code
                print('RAFATLink', program_url)
                yield scrapy.http.Request(program_url, cb_kwargs=kwargs, callback=self.parse_program)

    def parse_program(self, response, **kwargs):
        '''
        Parse the program details from the response.

        Args:
            response: The response object from the program URL request.
            **kwargs: Keyword arguments passed to the callback, containing the program code.

        Yields:
            Dictionary containing the program code, title, and description.
        '''
        program_code = kwargs['program_code']
        title = response.css('h1.uu-heading::text').get()
        description_section = response.css('div.sv-text-portlet-content p.uu-text::text').getall()
        description_text = ' '.join(description_section)

        yield {'program_code': program_code, 'title': title, 'description': description_text}
