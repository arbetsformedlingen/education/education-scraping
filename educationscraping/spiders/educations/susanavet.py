import datetime
import json
import logging
import re

import scrapy
from scrapy.utils.project import get_project_settings

from educationscraping.html_utils import clean_cdata
from educationscraping.items import EducationItem, EventItem, Execution, LangContent, CodeType, TuitionFee, \
    StartPeriod, ApplicationDetails, AdmissionDetails, Location, AdmissionCredit, \
    EducationProviderItem, Phone, Subject, Credits, AmountCurrency, Eligibility, Application, Distance, EventExtension

log = logging.getLogger(__name__)

settings = get_project_settings()


class SusanavetSpider(scrapy.Spider):
    # name prefix is used in file name for FEED_URI (e.g. 'education' or 'educationplans')
    feed_name_prefix = "educations"
    name = "susanavet"
    headers = {
        'Accept': 'application/json'
    }
    # start_date_str must be set if timestamp is used in filepath for FEED_URI:
    start_date_str = datetime.date.today().strftime("%Y-%m-%d")
    # Dict with all scraped education events to be added to educations
    cached_events = {}

    # Dict with all scraped education providers to be added to educations
    cached_education_providers = {}

    # Dict with all scraped education subjects to be added to educations
    cached_subjects = {}

    scraped_organization_forms = set()
    valid_but_missing_organization_forms = set()
    not_scraped_organization_forms = set()
    scraped_aub_types = set()
    not_scraped_aub_types = set()

    scraped_educations = set()

    # If educations should not be mapped into items structure.
    SKIP_ITEMS_FORMAT = settings['SKIP_ITEMS_FORMAT']

    API_URL = 'https://susanavet2.skolverket.se/api/1.1/'
    EVENTS_URL_PATTERN = 'events?size=%s&page=%s&organisationForm=%s'
    INFOS_URL_PATTERN = 'infos?size=%s&page=%s&organisationForm=%s'
    PROVIDERS_URL_PATTERN = 'providers?size=%s&page=%s&organisationForm=%s'
    SUBJECTS_URL_PATTERN = 'subjects?size=%s&page=%s'
    ORGANIZATION_FORMS_URL_PATTERN = 'reference/organisationForm'

    # Whitelist with valid organization forms to fetch:
    WHITELIST_ORGANIZATION_FORMS = ['aub', 'folkhögskola', 'grundläggande vuxenutbildning', 'gymnasial vuxenutbildning', 'högskoleutbildning',
                                    'konst- och kulturutbildning', 'utbildning med endast tillsyn', 'yrkeshögskoleutbildning']

    AUB_NAME_PREFIX = 'af'

    # Blacklist with invalid education forms that won't be scraped:
    BLACKLIST_EDUCATION_FORMS = [f"{AUB_NAME_PREFIX} test och kartläggning inför aub", f"{AUB_NAME_PREFIX} folkhögskola", f"{AUB_NAME_PREFIX} förberedande utbildning"]

    ITEMS_PER_PAGE = 1000

    # Used when developing to limit what organization form to scrape. If value is None, all organization forms will be scraped.
    ORGANIZATION_FORM = settings['ORGANIZATION_FORM']

    # Organization forms to iterate over in SUSA-navet.
    organization_forms_to_scrape = []

    def __init__(self, *args, **kwargs):
        log.info(f'Susanavet spider init with url {SusanavetSpider.API_URL}.')

        super(SusanavetSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        log.info('Susanavet start requests..')
        if self.SKIP_ITEMS_FORMAT:
            log.info('Skip to map educations into format specified in items...')
        else:
            log.info('Educations will be mapped into items structure...')
        organization_forms_url = self.API_URL + self.ORGANIZATION_FORMS_URL_PATTERN
        if not self.ORGANIZATION_FORM:
            yield scrapy.http.Request(organization_forms_url, headers=self.headers, callback=self.parse_organization_forms)
        else:
            self.organization_forms_to_scrape.append(self.ORGANIZATION_FORM)
            yield from self.start_events_scraping()


    def closed(self, reason):
        log.info(f'Spider closed: {self.name}, due to: {reason}')

        if self.scraped_organization_forms:
            log.info(f'The following valid organization forms were scraped: {sorted(self.scraped_organization_forms)}')
        if self.not_scraped_organization_forms:
            log.info(f'The following organization forms were NOT scraped (not in whitelist): {sorted(self.not_scraped_organization_forms)}')

        if self.scraped_aub_types:
            log.info(f'The following valid education forms were found (not blacklisted and found): {sorted(self.scraped_aub_types)}')
        if self.not_scraped_aub_types:
            log.info(f'The following education forms were NOT scraped (in blacklist): {sorted(self.not_scraped_aub_types)}')

        if self.valid_but_missing_organization_forms:
            log.warning(f'The following organization forms were configured as valid but are missing in the data: {sorted(self.valid_but_missing_organization_forms)}')



    def valid_date_format(self, input_date):
        # Validate if input date is YYYY-MM-DD or valid iso format to avoid future exceptions if indexing to elastic search...
        if not input_date:
            return False
        format = "%Y-%m-%d"
        date_str = str(input_date)
        try:
            datetime.datetime.strptime(date_str, format)
            return True
        except ValueError:
            pass
        try:
            datetime.fromisoformat(date_str)
            return True
        except:
            pass
        date_time_pattern = re.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}T([0-9]{2}:){2}[0-9]{2}")
        if re.match(date_time_pattern, date_str):
            return True
        else:
            log.warning(f"Not a valid date format for date {date_str}")
            return False

    @staticmethod
    def valid_municipaliy_code(code):
        if not code:
            return False

        # Convert values that look like integers to strings. Values starting with "0" is already str.
        if not len(str(code)) == 4:
            return False

        return all([char.isdigit() for char in str(code)])


    def parse_organization_forms(self, response):
        json_response = json.loads(response.body)
        if json_response:
            valid_organization_forms = [form for form in json_response if form.lower() in self.WHITELIST_ORGANIZATION_FORMS]
            log.info(f"Found valid organization forms: {sorted(valid_organization_forms)}")
            self.scraped_organization_forms.update(set(valid_organization_forms))

            not_valid_organization_forms = [form for form in json_response if form.lower() not in self.WHITELIST_ORGANIZATION_FORMS]
            if not_valid_organization_forms:
                self.not_scraped_organization_forms.update(set(not_valid_organization_forms))
                log.info(f"Found organization forms that will NOT be scraped (not in whitelist): {sorted(self.not_scraped_organization_forms)}")

            valid_sorted_organization_forms = sorted([form.lower() for form in valid_organization_forms])
            whitelist_sorted_organization_forms = sorted(self.WHITELIST_ORGANIZATION_FORMS)

            if valid_sorted_organization_forms != whitelist_sorted_organization_forms:
                valid_but_missing = set(self.WHITELIST_ORGANIZATION_FORMS) - set(valid_sorted_organization_forms)
                log.warning(f"The following organization forms are configured as valid but missing in the data: {sorted(valid_but_missing)}")
                self.valid_but_missing_organization_forms.update(valid_but_missing)

            for organization_form in valid_organization_forms:
                self.organization_forms_to_scrape.append(organization_form)
        elif not json_response or len(self.organization_forms_to_scrape) < 1:
            log.warning("Didn't find any organization forms")

        yield from self.start_events_scraping()

    def start_events_scraping(self):
        events_url = self.API_URL + (self.EVENTS_URL_PATTERN % (self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[0]))
        yield scrapy.http.Request(events_url, headers=self.headers, callback=self.parse_events,
                                  meta={'organization_form_index': 0})

    def map_lang_content(self, input):
        lang_content_arr = []
        try:
            if input:
                if input.get('string', None):
                    input_string = input['string']
                    for lang_content in input_string:
                        lang_content_item = LangContent()
                        lang_content_item['lang'] = lang_content.get('lang', None)
                        lang_content_item['content'] = clean_cdata(lang_content.get('content', None))
                        lang_content_arr.append(lang_content_item)
        except Exception as e:
            log.debug(f"Error in map_lang_content with input: {input}")
            log.error(e)
        return lang_content_arr

    def map_lang_content_from_arr(self, input_arr):
        # Takes in data in following format:
        # input_arr: [
        #       "string": [ "str" ],
        #        "lang": "swe"
        # ]
        # Output: [LangContent]
        lang_content_arr = []
        try:
            if input_arr:
                for input in input_arr:
                    lang = input.get('lang', None)
                    if input.get('string', None):
                        string_arr = input['string']
                        for str in string_arr:
                            lang_content_item = LangContent()
                            lang_content_item['lang'] = lang
                            lang_content_item['content'] = clean_cdata(str)
                            lang_content_arr.append(lang_content_item)
        except Exception as e:
            log.debug(f"Error in map_lang_content_from_arr with input: {input_arr}")
            log.error(e)
        return lang_content_arr

    def map_application(self, input):
        application = Application()
        try:
            if input:
                if isinstance(input, dict):
                    application = Application()
                    if input.get('code', None):
                        application['code'] = input['code']
                    if input.get('last', None):
                        application['last'] = input['last']
                    if input.get('first', None):
                        application['first'] = input['first']
                    if input.get('instruction', None):
                        instruction = input['instruction']
                        application['instruction'] = self.map_lang_content(instruction)

                    if input.get('url', None):
                        url = input['url']
                        if isinstance(url, dict) and url.get('url', None):
                            urls = url['url']
                            if isinstance(urls, list):
                                url_items = []
                                for lang_content in urls:
                                    lang_content_item = LangContent()
                                    if lang_content.get('lang', None):
                                        lang_content_item['lang'] = lang_content['lang']
                                    if lang_content.get('content', None):
                                        lang_content_item['content'] = lang_content['content']

                                    url_items.append(lang_content_item)
                                application['urls'] = url_items
                else:
                    log.warning(f"Warning in map_application: unexpected type {input}")
        except Exception as e:
            log.debug(f"Error in map_application with input: {input}")
            log.error(e)
        return application

    def map_code_type(self, input, code_lower_case=False, type_lower_case=False):
        code_type = CodeType()
        try:
            if input:
                if input.get('code', None):
                    if code_lower_case:
                        code_type['code'] = str(input['code']).lower()
                    else:
                        code_type['code'] = str(input['code'])
                if input.get('type', None):
                    if type_lower_case:
                        code_type['type'] = input['type'].lower()
                    else:
                        code_type['type'] = input['type']
        except Exception as e:
            log.debug(f"Error in map_code_type with input: {input}")
            log.error(e)
        return code_type

    def map_code_type_arr(self, input_arr):
        code_type_arr = []
        try:
            if input_arr and isinstance(input_arr, list) and len(input_arr) > 0:
                for input in input_arr:
                    code_type_arr.append(self.map_code_type(input))
        except Exception as e:
            log.debug(f"Error in map_code_type_arr with input: {input_arr}")
            log.error(e)
        return code_type_arr

    def map_credits(self, input):
        credits = Credits()
        try:
            if input:
                if input.get('system', None):
                    code_type = self.map_code_type_arr_first_element(input['system'])
                    credits['system'] = code_type
                if input.get('credits', None):
                    credit_arr = input['credits']
                    if isinstance(credit_arr, list) and len(credit_arr) > 0:
                        credits['credits'] = credit_arr[0]
                        if len(credit_arr) > 1:
                            log.warning(f"Warning in map_credits: list contains more than 1 element {credit_arr}")
        except Exception as e:
            log.debug(f"Error in map_credits with input: {input}")
            log.error(e)
        return credits

    def map_eligibility(self, input):
        eligibility_item = Eligibility()
        try:
            if input:
                if input.get('eligibilityDescription', None):
                    eligibility_descriptions = input['eligibilityDescription']
                    if isinstance(eligibility_descriptions, list):
                        eligibility_arr = []
                        for eligibility_description in eligibility_descriptions:
                            eligibility_arr.append(self.map_lang_content(eligibility_description))

                        eligibility_item['eligibilityDescription'] = eligibility_arr
                    else:
                        log.warning(
                            f"Warning in map_eligibility eligibilityDescription: unexpected type {eligibility_descriptions}")

                if input.get('additionDescription', None):
                    addition_descriptions = input['additionDescription']
                    if isinstance(addition_descriptions, list):
                        addition_description_arr = []
                        for addition_description in addition_descriptions:
                            addition_description_arr.append(self.map_lang_content(addition_description))
                        eligibility_item['additionDescription'] = addition_description_arr
                    else:
                        log.warning(
                            f"Warning in map_eligibility additionDescriptions: unexpected type {addition_descriptions}")
        except Exception as e:
            log.debug(f"Error in map_eligibility with input: {input}")
            log.error(e)
        return eligibility_item

    def map_code_type_arr_first_element(self, input_arr):
        code_type = None
        try:
            if input_arr and isinstance(input_arr, list) and len(input_arr) > 0:
                code_type = self.map_code_type(input_arr[0])
                if len(input_arr) > 1:
                    log.warning(
                        f"Warning in map_code_type_arr_first_element: list contains more than 1 element {input_arr}")
        except Exception as e:
            log.debug(f"Error in map_code_type_arr_first_element with input: {input_arr}")
            log.error(e)
        return code_type

    def fetch_first_element(self, input_arr):
        if input_arr and isinstance(input_arr, list) and len(input_arr) > 0:
            if len(input_arr) > 1:
                log.warning(f"Warning in fetch_first_element: list contains more than 1 element {input_arr}")
            return input_arr[0]
        elif isinstance(input_arr, dict):
            return input_arr

    def map_execution(self, input):
        exection = Execution()
        try:
            if input:
                if input.get('condition', None):
                    exection['condition'] = input['condition']
                if input.get('start', None) and self.valid_date_format(input['start']):
                    exection['start'] = str(input['start'])
                if input.get('end', None) and self.valid_date_format(input['end']):
                    exection['end'] = str(input['end'])
        except Exception as e:
            log.debug(f"Error in map_execution with input: {input}")
            log.error(e)
        return exection

    def map_tuition_fee(self, input):
        tuition_fee = TuitionFee()
        try:
            if input:
                if isinstance(input, list) and len(input) > 0:
                    tuition_fee_input = input[0]
                    if len(input) > 1:
                        log.warning(f"Warning in map_tuition_fee: list contains more than 1 element {input}")
                    if isinstance(tuition_fee_input, dict):
                        if tuition_fee_input.get('total', None):
                            tuition_fee_total = tuition_fee_input['total']
                            if isinstance(tuition_fee_total, list) and len(tuition_fee_total) > 0:
                                tuition_fee['total'] = tuition_fee_total[0]
                                if len(tuition_fee_total) > 1:
                                    log.warning(
                                        f"Warning in map_tuition_fee: list contains more than 1 element {tuition_fee_total}")
                            elif not isinstance(tuition_fee_total, list):
                                tuition_fee['total'] = tuition_fee_input['total']

                        if tuition_fee_input.get('content', None):
                            tuition_fee_content = tuition_fee_input['content']
                            if isinstance(tuition_fee_content, list) and len(tuition_fee_content) > 0:
                                tuition_fee['content'] = tuition_fee_content[0]
                                if len(tuition_fee_content) > 1:
                                    log.warning(
                                        f"Warning in map_tuition_fee: list contains more than 1 element {tuition_fee_content}")
                            elif not isinstance(tuition_fee_input, list):
                                tuition_fee['content'] = tuition_fee_input['content']
                        if tuition_fee_input.get('first', None):
                            tuition_fee_first = tuition_fee_input['first']
                            if isinstance(tuition_fee_first, list) and len(tuition_fee_first) > 0:
                                tuition_fee['first'] = tuition_fee_first[0]
                                if len(tuition_fee_first) > 1:
                                    log.warning(
                                        f"Warning in map_tuition_fee: list contains more than 1 element {tuition_fee_first}")
                            elif not isinstance(tuition_fee_first, list):
                                tuition_fee['first'] = tuition_fee_input['first']
        except Exception as e:
            log.debug(f"Error in map_tuition_fee with input: {input}")
            log.error(e)
        return tuition_fee

    def map_start_period(self, input):
        start_period = StartPeriod()
        try:
            if input and isinstance(input, list) and len(input) > 0:
                start_period_input = input[0]
                if len(input) > 1:
                    log.warning(f"Warning in map_start_period: list contains more than 1 element {input}")
                if isinstance(start_period_input, dict):
                    if start_period_input.get('period', None):
                        periods = start_period_input['period']
                        if isinstance(periods, list) and len(periods) > 0:
                            period = periods[0]
                            if len(periods) > 1:
                                log.warning(f"Warning in map_start_period: list contains more than 1 element {periods}")
                            if period.get('periodNumber', None):
                                period_numbers = period['periodNumber']
                                if isinstance(period_numbers, list) and len(period_numbers) > 0:
                                    start_period['periodNumber'] = period_numbers[0]
                                    if len(period_numbers) > 1:
                                        log.warning(
                                            f"Warning in map_start_period: list contains more than 1 element {period_numbers}")
                                elif not isinstance(period_numbers, list):
                                    start_period['periodNumber'] = period_numbers
                            if period.get('year', None):
                                years = period['year']
                                if isinstance(years, list) and len(years) > 0:
                                    start_period['year'] = years[0]
                                    if len(years) > 1:
                                        log.warning(
                                            f"Warning in map_start_period: list contains more than 1 element {years}")
                                elif not isinstance(years, list):
                                    start_period['year'] = years
                            if period.get('semester', None):
                                vacations = period['semester']
                                if isinstance(vacations, list) and len(vacations) > 0:
                                    start_period['vacation'] = vacations[0]
                                    if len(vacations) > 1:
                                        log.warning(
                                            f"Warning in map_start_period: list contains more than 1 element {vacations}")
                                elif not isinstance(vacations, list):
                                    start_period['vacation'] = vacations
                    if start_period_input.get('week', None):
                        weeks = start_period_input['week']
                        if isinstance(weeks, list) and len(weeks) > 0:
                            start_period['week'] = weeks[0]
                            if len(weeks) > 1:
                                log.warning(f"Warning in map_start_period: list contains more than 1 element {weeks}")
                        elif not isinstance(weeks, list):
                            start_period['week'] = weeks
        except Exception as e:
            log.debug(f"Error in map_start_period with input: {input}")
            log.error(e)
        return start_period

    def map_application_details(self, input):
        application_detail = ApplicationDetails()
        try:
            if input and isinstance(input, list) and len(input) > 0:
                application_details_input = input[0]
                if len(input) > 1:
                    log.warning(f"Warning in map_application_details: list contains more than 1 element {input}")
                if isinstance(application_details_input, dict):
                    if application_details_input.get('application_details_input', None):
                        close_dates = application_details_input['application_details_input']
                        if isinstance(close_dates, list) and len(close_dates) > 0:
                            application_detail['closeDate'] = close_dates[0]
                            if len(close_dates) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {close_dates}")
                        elif not isinstance(close_dates, list):
                            application_detail['closeDate'] = close_dates
                    if application_details_input.get('applicationModel', None):
                        application_models = application_details_input['applicationModel']
                        if isinstance(application_models, list) and len(application_models) > 0:
                            application_detail['applicationModel'] = application_models[0]
                            if len(application_models) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {application_models}")
                        elif not isinstance(application_models, list):
                            application_detail['applicationModel'] = application_models
                    if application_details_input.get('visibleToInternationalApplicants', None):
                        visible_to_internal_applicants = application_details_input['visibleToInternationalApplicants']
                        if isinstance(visible_to_internal_applicants, list) and len(visible_to_internal_applicants) > 0:
                            application_detail['visibleToInternationalApplicants'] = visible_to_internal_applicants[0]
                            if len(visible_to_internal_applicants) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {visible_to_internal_applicants}")
                        elif not isinstance(visible_to_internal_applicants, list):
                            application_detail['visibleToInternationalApplicants'] = visible_to_internal_applicants
                    if application_details_input.get('visibleToSwedishApplicants', None):
                        visible_to_swedish_applicants = application_details_input['visibleToSwedishApplicants']
                        if isinstance(visible_to_swedish_applicants, list) and len(visible_to_swedish_applicants) > 0:
                            application_detail['visibleToSwedishApplicants'] = visible_to_swedish_applicants[0]
                            if len(visible_to_swedish_applicants) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {visible_to_swedish_applicants}")
                        elif not isinstance(visible_to_swedish_applicants, list):
                            application_detail['visibleToSwedishApplicants'] = visible_to_swedish_applicants
                    if application_details_input.get('onlyAsPartOfProgram', None):
                        only_part_of_programs = application_details_input['onlyAsPartOfProgram']
                        if isinstance(only_part_of_programs, list) and len(only_part_of_programs) > 0:
                            application_detail['onlyAsPartOfProgram'] = only_part_of_programs[0]
                            if len(only_part_of_programs) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {only_part_of_programs}")
                        elif not isinstance(only_part_of_programs, list):
                            application_detail['onlyAsPartOfProgram'] = only_part_of_programs
        except Exception as e:
            log.debug(f"Error in map_application_details with input: {input}")
            log.error(e)
        return application_detail

    def map_admission_details(self, input):
        admission_detail = AdmissionDetails()
        admission_detail_credits = []
        try:
            if input and isinstance(input, list) and len(input) > 0:
                admission_details_input = input[0]
                if len(input) > 1:
                    log.warning(f"Warning in map_application_details: list contains more than 1 element {input}")
                if isinstance(admission_details_input, dict):
                    if admission_details_input.get('selectionModel', None):
                        selection_models = admission_details_input['selectionModel']
                        if isinstance(selection_models, list) and len(selection_models) > 0:
                            admission_detail['selectionModel'] = selection_models[0]
                            if len(selection_models) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {selection_models}")
                        elif not isinstance(selection_models, list):
                            admission_detail['selectionModel'] = selection_models
                    if admission_details_input.get('distributionOfCredits', None):
                        distribution_of_credits = admission_details_input['distributionOfCredits']
                        if isinstance(distribution_of_credits, list) and len(distribution_of_credits) > 0:
                            distribution_of_credit = distribution_of_credits[0]
                            if len(distribution_of_credits) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {distribution_of_credits}")
                            if distribution_of_credit.get('credit', None):
                                credits_arr = distribution_of_credit['credit']
                                if isinstance(credits_arr, list) and len(credits_arr) > 0:
                                    for credit in credits_arr:
                                        admissionCredit = AdmissionCredit()
                                        if credit.get('startWeek', None):
                                            start_weeks = credit['startWeek']
                                            if isinstance(start_weeks, list) and len(start_weeks) > 0:
                                                admissionCredit['startWeek'] = start_weeks[0]
                                                if len(start_weeks) > 1:
                                                    log.warning(
                                                        f"Warning in map_application_details: list contains more than 1 element {start_weeks}")
                                            elif not isinstance(start_weeks, list):
                                                admissionCredit['startWeek'] = start_weeks
                                        if credit.get('startWeek', None):
                                            stop_weeks = credit['stopWeek']
                                            if isinstance(stop_weeks, list) and len(stop_weeks) > 0:
                                                admissionCredit['stopWeek'] = stop_weeks[0]
                                                if len(stop_weeks) > 1:
                                                    log.warning(
                                                        f"Warning in map_application_details: list contains more than 1 element {stop_weeks}")
                                            elif not isinstance(stop_weeks, list):
                                                admissionCredit['stopWeek'] = stop_weeks
                                        if credit.get('year', None):
                                            years = credit['year']
                                            if isinstance(years, list) and len(years) > 0:
                                                admissionCredit['year'] = years[0]
                                                if len(years) > 1:
                                                    log.warning(
                                                        f"Warning in map_application_details: list contains more than 1 element {years}")
                                            elif not isinstance(years, list):
                                                admissionCredit['year'] = years
                                        if credit.get('semester', None):
                                            semester = credit['semester']
                                            if isinstance(semester, list) and len(semester) > 0:
                                                admissionCredit['vacation'] = semester[0]
                                                if len(semester) > 1:
                                                    log.warning(
                                                        f"Warning in map_application_details: list contains more than 1 element {semester}")
                                            elif not isinstance(semester, list):
                                                admissionCredit['vacation'] = semester
                                        if credit.get('content', None):
                                            contents = credit['content']
                                            if isinstance(contents, list) and len(contents) > 0:
                                                admissionCredit['content'] = contents[0]
                                                if len(contents) > 1:
                                                    log.warning(
                                                        f"Warning in map_application_details: list contains more than 1 element {contents}")
                                            elif not isinstance(contents, list):
                                                admissionCredit['content'] = contents
                                        admission_detail_credits.append(admissionCredit)
                    if admission_details_input.get('credentialRatingModel', None):
                        credential_rating_models = admission_details_input['credentialRatingModel']
                        if isinstance(credential_rating_models, list) and len(credential_rating_models) > 0:
                            credential_rating_model = credential_rating_models[0]
                            if len(credential_rating_models) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {credential_rating_models}")
                            if isinstance(credential_rating_model, str):
                                admission_detail['credentialRatingModel'] = credential_rating_model
                            elif isinstance(credential_rating_model, int):
                                admission_detail['credentialRatingModel'] = str(credential_rating_model)
                            elif isinstance(credential_rating_model, dict) and credential_rating_model.get('useNoModel',
                                                                                                           None):
                                admission_detail['credentialRatingModelUseNoModel'] = credential_rating_model[
                                    'useNoModel']
                            else:
                                log.warning(
                                    f"credential_rating_model contains unknown value/type: {credential_rating_model}/{type(credential_rating_model)}")
                        elif isinstance(credential_rating_models, str):
                            admission_detail['credentialRatingModel'] = credential_rating_models
                        elif isinstance(credential_rating_models, int):
                            admission_detail['credentialRatingModel'] = str(credential_rating_models)
                        elif isinstance(credential_rating_models, dict) and credential_rating_models.get('useNoModel',
                                                                                                         None):
                            admission_detail['credentialRatingModelUseNoModel'] = credential_rating_models['useNoModel']
                        else:
                            log.warning(
                                f"credential_rating_models contains unknown value/type: {credential_rating_models}/{type(credential_rating_models)}")
                    if admission_details_input.get('eligibilityModelSB', None):
                        eligibility_models_sb = admission_details_input['eligibilityModelSB']
                        if isinstance(eligibility_models_sb, list) and len(eligibility_models_sb) > 0:
                            eligibility_model_sb = eligibility_models_sb[0]
                            if len(eligibility_models_sb) > 1:
                                log.warning(
                                    f"Warning in map_application_details: list contains more than 1 element {eligibility_models_sb}")
                            if isinstance(eligibility_model_sb, str):
                                admission_detail['eligibilityModelSB'] = eligibility_model_sb
                            elif isinstance(eligibility_model_sb, int):
                                admission_detail['eligibilityModelSB'] = str(eligibility_model_sb)
                            elif isinstance(eligibility_model_sb, dict) and eligibility_model_sb.get('useNoModel',
                                                                                                     None):
                                admission_detail['eligibilityModelSBUseNoModel'] = eligibility_model_sb['useNoModel']
                            else:
                                log.warning(f"eligibility_model_sb contains unknown value/type: {eligibility_model_sb}/{type(eligibility_model_sb)}")
                        elif isinstance(eligibility_models_sb, str):
                            admission_detail['eligibilityModelSB'] = eligibility_models_sb
                        elif isinstance(eligibility_models_sb, dict) and eligibility_models_sb.get('useNoModel', None):
                            admission_detail['eligibilityModelSBUseNoModel'] = eligibility_models_sb['useNoModel']
                        else:
                            log.warning(f"eligibility_models_sb contains unknown value/type: {eligibility_models_sb}/{type(eligibility_models_sb)}")
        except Exception as e:
            log.debug(f"Error in map_application_details with input: {input}")
            log.error(e)
        return admission_detail

    def map_event_package_references(self, input):
        education_event_ref_arr = []
        try:
            if input and isinstance(input, list) and len(input) > 0:
                event_package_references = input[0]
                if len(input) > 1:
                    log.warning(f"Warning in map_event_package_references: list contains more than 1 element {input}")
                if isinstance(event_package_references, dict):
                    if event_package_references.get('educationEventRef', None):
                        education_event_refs = event_package_references['educationEventRef']
                        if isinstance(education_event_refs, list):
                            for education_event_ref in education_event_refs:
                                if education_event_ref.get(id):
                                    ids = education_event_ref['id']
                                    if isinstance(ids, list) and len(ids) > 1:
                                        education_event_ref_arr.append(ids[0])
            elif input:
                log.warning(f"Warning in map_event_package_references: unexpected data {input}")
        except Exception as e:
            log.debug(f"Error in map_event_package_references with input: {input}")
            log.error(e)
        return education_event_ref_arr

    @staticmethod
    def _parse_location(input):
        location = Location()
        try:
            if input.get('town', None):
                location['town'] = input['town']
            if input.get('streetAddress', None):
                location['streetAddress'] = input['streetAddress']
            if input.get('postBox', None):
                post_box = input['postBox']
                if isinstance(post_box, list):
                    location['postBox'] = post_box[0]
                else:
                    location['postBox'] = post_box
            if input.get('postCode', None):
                location['postCode'] = str(input['postCode'])
            if input.get('country', None):
                location['country'] = input['country']
            # Validate that the municipality code is four digits.
            if SusanavetSpider.valid_municipaliy_code(input.get('municipalityCode', None)):
                location['municipalityCode'] = str(input['municipalityCode'])
                # Derive regionCode from municipalityCode
                location['regionCode'] = str(location['municipalityCode'][:2])
        except Exception as e:
            log.debug(f"Error parsing location with input: {input}")
            log.error(e)

        return location


    def map_locations(self, input):
        locations = []
        if input and isinstance(input, list):
            for location_input in input:
                location = SusanavetSpider._parse_location(location_input)
                if location: locations.append(location)
        return locations


    def map_location(self, input):
        location_input = None
        if input and isinstance(input, list):
            location_input = input[0]
            if len(input) > 1:
                log.warning(f"Warning in map_location: list contains more than 1 element {input}")
        else:
            location_input = input

        if location_input:
            location = SusanavetSpider._parse_location(location_input)
            return location


    def map_education(self, education_json):
        if self.SKIP_ITEMS_FORMAT:
            return education_json
        education = EducationItem()
        try:
            education_content = education_json.get('content', None)
            if education_content:
                education_info = education_content.get('educationInfo', None)
                if education_info:
                    education['identifier'] = education_info.get('identifier', None).lower()
                    education['resultIsDegree'] = education_info.get('resultIsDegree', None)  # boolean
                    education['expires'] = education_info.get('expires', None)  # boolean
                    education['recommendedPriorKnowledge'] = self.map_lang_content(
                        education_info.get('recommendedPriorKnowledge', None))  # LangContent
                    education['code'] = education_info.get('code', None)
                    education['configuration'] = self.map_code_type(
                        education_info.get('configuration', None))  # CodeType
                    if education_info.get('subject', None):
                        subjects_arr = education_info['subject']
                        if isinstance(subjects_arr, list):
                            subjects = []
                            for subject_item in subjects_arr:
                                if isinstance(subject_item, dict) and subject_item.get('code', None):
                                    subject_code = str(subject_item['code'])
                                    # If subject code find in cache...
                                    if self.cached_subjects.get(subject_code):
                                        subjects.append(self.map_subject(self.cached_subjects[subject_code]))
                                    else:
                                        # Subject code not found in cache. Use code/type from current education...
                                        subjects.append(self.map_subject(subject_item))
                        education['subject'] = subjects
                    education['description'] = self.map_lang_content(
                        education_info.get('description', None))  # CodeType

                    if education_info.get('eligibility'):
                        education['eligibility'] = self.map_eligibility(education_info['eligibility'])
                    education['lastEdited'] = education_info.get('lastEdited',
                                                                 None)  # TODO Dateformat 2021-03-18T14:47:37.162+01:00
                    education['title'] = self.map_lang_content(education_info.get('title', None))  # LangContent []
                    # Make sure all education form codes (e.g. AUB, folkhögskola) are lower case to minimize problems in later steps...
                    education['form'] = self.map_code_type(education_info.get('form', None), True, False)  # CodeType

                    # For AUB, use aub_type in extension section as education_form...
                    if education['form'].get('code'):
                        if education['form']['code'] == 'aub':
                            if education_info.get('extension'):
                                education_extensions = education_info['extension']
                                if isinstance(education_extensions, list) and len(education_extensions) > 0:
                                    first_extension = education_extensions[0]
                                    if first_extension.get('aub_type'):
                                        aub_types = first_extension['aub_type']
                                        if isinstance(aub_types, list) and len(aub_types) > 0:
                                            first_aub_type = aub_types[0]
                                            if isinstance(first_aub_type, str):
                                                first_aub_type = first_aub_type.lower()
                                                education['form']['code'] = f"{self.AUB_NAME_PREFIX} {first_aub_type}"

                    education['isVocational'] = education_info.get('isVocational', None)
                    education['credits'] = self.map_credits(education_info.get('credits', None))
                    education['educationLevel'] = self.map_code_type_arr_first_element(
                        education_info.get('educationLevel', None))  # CodeType
                    if education_info.get('url', None):
                        url_dict = education_info['url']
                        if isinstance(url_dict, dict) and url_dict.get('url', None):
                            urls = url_dict['url']
                            if isinstance(urls, list) and len(urls) > 0:
                                first_url = urls[0]
                                if len(urls) > 1:
                                    log.warning(f"Warning in map_education: list contains more than 1 element {urls}")
                                education['urls'] = self.map_lang_content(first_url)
                            elif not isinstance(urls, list):
                                log.warning(f"Warning in map_education urls: unexpected type {urls}")
                        elif not isinstance(url_dict, dict):
                            log.warning(f"Warning in map_education urls: unexpected type {url_dict}")
                    if education_info.get('eligibleForStudentAid', None):
                        education['eligibleForStudentAid'] = self.map_code_type(
                            education_info.get('eligibleForStudentAid'))

        except Exception as e:
            log.debug(f"Error in map_education with input: {education_json}")
            log.error(e)
        return education

    def map_subject(self, subject_content):
        subject = Subject()
        try:
            if subject_content and isinstance(subject_content, dict):
                if subject_content.get('code', None):
                    subject['code'] = subject_content['code']
                if subject_content.get('name', None):
                    subject['name'] = subject_content['name']
                if subject_content.get('nameEn', None):
                    subject['nameEn'] = subject_content['nameEn']
                if subject_content.get('type', None):
                    subject['type'] = subject_content['type']
        except Exception as e:
            log.debug(f"Error in map_subject with input: {subject_content}")
            log.error(e)
        return subject

    def map_provider(self, provider_content):
        if self.SKIP_ITEMS_FORMAT:
            return provider_content
        provider = EducationProviderItem()
        try:
            provider['identifier'] = provider_content.get('identifier', None).lower()
            provider['expires'] = provider_content.get('expires', None)
            if provider_content.get('year', None):
                provider['year'] = self.map_code_type_arr(provider_content['year'])

            if provider_content.get('responsibleBody', None):
                responsible_body = provider_content['responsibleBody']
                if responsible_body.get('name', None):
                    responsible_body_name = responsible_body['name']
                    provider['responsibleBody'] = self.map_lang_content(responsible_body_name)
                provider['responsibleBodyType'] = self.map_code_type(provider_content.get('type', None))

            provider['emailAddress'] = provider_content.get('emailAddress', None)
            if provider_content.get('phone', None):
                phone_arr = provider_content['phone']
                if isinstance(phone_arr, list):
                    phones = []
                    for phone_number in phone_arr:
                        phone = Phone()
                        if isinstance(phone_number, dict) and phone_number.get('number', None):
                            phone['number'] = str(phone_number['number'])
                        if isinstance(phone_number, dict) and phone_number.get('function', None):
                            function = phone_number['function']
                            phone['function'] = self.map_lang_content(function)
                        phones.append(phone)
                    provider['phone'] = phones
            provider['name'] = self.map_lang_content(provider_content.get('name', None))
            provider['contactAddress'] = self.map_location(provider_content.get('contactAddress', None))
            provider['visitAddress'] = self.map_location(provider_content.get('visitAddress', None))
            if provider_content.get('url', None):
                urls = provider_content['url']
                if isinstance(urls, list) and len(urls) > 0:
                    first_url = urls[0]
                    if isinstance(urls, list) and len(urls) > 1:
                        log.warning(f"Warning in map_provider urls: list contains more than 1 element {urls}")
                    if first_url.get('url', None):
                        provider_urls = first_url['url']
                        if isinstance(provider_urls, list):
                            lang_content_arr = []
                            for provider_url in provider_urls:
                                lang_content_item = LangContent()
                                lang_content_item['lang'] = provider_url.get('lang', None)
                                lang_content_item['content'] = provider_url.get('content', None)
                                lang_content_arr.append(lang_content_item)
                            provider['urls'] = lang_content_arr
                        else:
                            log.warning(f"Warning in map_provider urls: unexpected type {provider_urls}")
        except Exception as e:
            log.debug(f"Error in map_provider with input: {provider_content}")
            log.error(e)
        return provider

    def map_event(self, event_content):
        if self.SKIP_ITEMS_FORMAT:
            return event_content
        event = EventItem()
        try:
            event['identifier'] = event_content.get('identifier', None).lower()
            event['execution'] = self.map_execution(event_content.get('execution', None))  # Execution
            if event_content.get('expires', None) and self.valid_date_format(event_content['expires']):
                event['expires'] = str(event_content.get('expires', None))  # 2022-09-30T00:00:00.000+02:00
            if event_content.get('cancelled', None):
                event['cancelled'] = event_content.get('cancelled', None)  # boolean
            elif event_content.get('isCancelled', None):
                event['cancelled'] = event_content.get('isCancelled', None)  # boolean
            if event_content.get('fee', None):
                fees = event_content['fee']
                if isinstance(fees, list) and len(fees) > 0:
                    fee_amount = AmountCurrency()
                    fee = fees[0]
                    if fee.get('amount', None):
                        amounts = fee['amount']
                        if isinstance(amounts, list) and len(amounts) > 0:
                            fee_amount['amount'] = amounts[0]
                            if len(amounts) > 1:
                                log.warning(f"Warning in map_event fee: list contains more than 1 element {amounts}")
                    if fee.get('currency', None):
                        currencies = fee['currency']
                        if isinstance(currencies, list) and len(currencies) > 0:
                            fee_amount['currency'] = currencies[0]
                            if len(currencies) > 1:
                                log.warning(f"Warning in map_event fee: list contains more than 1 element {currencies}")
                    if len(fees) > 1:
                        log.warning(f"Warning in map_event fee: list contains more than 1 element {fees}")
                    event['fee'] = fee_amount
                elif not isinstance(fees, list):
                    log.warning(f"Warning in map_event fee: unexpected type {fees}")

            try:
                if event_content.get('extension', None):
                    extension_arr = event_content['extension']
                    if isinstance(extension_arr, list) and len(extension_arr) > 0:
                        extension = extension_arr[0]
                        if len(extension_arr) > 1:
                            log.warning(
                                f"Warning in map_event extension: list contains more than 1 element {extension_arr}")
                        if isinstance(extension, dict):
                            event_extension = EventExtension()
                            try:
                                if extension.get('stopWeek', None):
                                    stop_week_arr = extension['stopWeek']
                                    if isinstance(stop_week_arr, list) and len(stop_week_arr) > 0:
                                        stop_week = stop_week_arr[0]
                                        if len(stop_week_arr) > 1:
                                            log.warning(
                                                f"Warning in map_event extension stop_week: list contains more than 1 element {stop_week_arr}")
                                        if stop_week.get('week', None):
                                            week_arr = stop_week['week']
                                            if isinstance(week_arr, list) and len(week_arr) > 0:
                                                event_extension['stopWeek'] = week_arr[0]
                                                if len(week_arr) > 1:
                                                    log.warning(
                                                        f"Warning in map_event extension week_arr: list contains more than 1 element {week_arr}")
                            except Exception as e_stop_week:
                                log.debug(f"Error in map_event extension stop_week input: {event_extension}")
                                log.error(e_stop_week)
                            if extension.get('keywords', None):
                                keywords = extension['keywords']
                                if isinstance(keywords, list) and len(keywords) > 0:
                                    keyword = keywords[0]
                                    if len(keywords) > 1:
                                        log.warning(
                                            f"Warning in map_event extension keywords: list contains more than 1 element {keywords}")
                                    event_extension['keywords'] = self.map_lang_content_from_arr(
                                        keyword.get('words', None))
                                elif isinstance(keywords, dict):
                                    event_extension['keywords'] = self.map_lang_content_from_arr(
                                        keywords.get('words', None))

                            event_extension['tuitionFee'] = self.map_tuition_fee(
                                extension.get('tuitionFee', None))  # TuitionFee
                            event_extension['itdistance'] = extension.get('itdistance', None)  # boolean
                            event_extension['formOfFunding'] = extension.get('formOfFunding', None)  # string []
                            event_extension['admissionDetails'] = self.map_admission_details(
                                extension.get('admissionDetails', None))
                            if extension.get('genericReference', None):
                                generic_references = extension['genericReference']
                                if isinstance(generic_references, list) and len(generic_references) > 0:
                                    generic_reference = generic_references[0]
                                    if isinstance(generic_references, list) and len(generic_references) > 1:
                                        log.warning(
                                            f"Warning in map_event extension generic_references: list contains more than 1 element {generic_references}")
                                    if generic_reference.get('uniqueIdentifier', None):
                                        uniqueIdentifiers = generic_reference['uniqueIdentifier']
                                        if isinstance(uniqueIdentifiers, list) and len(uniqueIdentifiers) > 0:
                                            event_extension['uniqueIdentifier'] = uniqueIdentifiers[0]
                                            if len(uniqueIdentifiers) > 1:
                                                log.warning(
                                                    f"Warning in map_event extension unique_identifiers: list contains more than 1 element {uniqueIdentifiers}")
                                        elif not isinstance(uniqueIdentifiers, list):
                                            event_extension['uniqueIdentifier'] = uniqueIdentifiers
                            event_extension['applicationDetails'] = self.map_application_details(
                                extension.get('applicationDetails', None))  # ApplicationDetails
                            event_extension['id'] = extension.get('id', None)
                            event_extension['type'] = extension.get('type', None)
                            event_extension['startPeriod'] = self.map_start_period(extension.get('startPeriod', None))
                            event_extension['eligibilityExemption'] = self.fetch_first_element(
                                extension.get('eligibilityExemption', None))
                            event_extension['department'] = self.fetch_first_element(extension.get('department', None))
                            event_extension['educationEventRef'] = self.map_event_package_references(
                                extension.get('eventPackageReferences', None))
                            event['extension'] = event_extension
            except Exception as e1:
                log.debug(f"Error in map_event extension with input: {event_content}")
                log.error(e1)
            event['education'] = event_content.get('education', None)

            if event_content.get('distance', None):
                distance = event_content['distance']
                if isinstance(distance, dict):
                    distance_item = Distance()
                    if distance.get('mandatory', None):
                        mandatories = distance['mandatory']
                        if isinstance(mandatories, list) and len(mandatories) > 0:
                            mandatory = mandatories[0]
                            if len(mandatories) > 1:
                                log.warning(
                                    f"Warning in map_event distance: list contains more than 1 element {mandatories}")
                            if isinstance(mandatory, dict) and mandatory.get('count', None):
                                mandatory_counts = mandatory['count']
                                if isinstance(mandatory_counts, list) and len(mandatory_counts) > 0:
                                    distance_item['mandatory'] = mandatory_counts[0]
                                    if len(mandatory_counts) > 1:
                                        log.warning(
                                            f"Warning in map_event distance: list contains more than 1 element {mandatory_counts}")
                            else:
                                log.warning(f"Warning in map_event distance: unexpected type {mandatory}")
                    if isinstance(distance, dict) and distance.get('optional'):
                        optionals = distance['optional']
                        if isinstance(optionals, list) and len(optionals) > 0:
                            optional = optionals[0]
                            if isinstance(optional, list) and len(optional) > 1:
                                log.warning(
                                    f"Warning in map_event distance: list contains more than 1 element {optional}")
                            if isinstance(optional, dict) and optional.get('count', None):
                                optional_counts = optional['count']
                                if isinstance(optional_counts, list) and len(optional_counts) > 0:
                                    distance_item['optional'] = optional_counts[0]
                    if isinstance(distance, dict) and distance.get('mandatoryNet'):
                        mandatory_nets = distance['mandatoryNet']
                        if isinstance(mandatory_nets, list) and len(mandatory_nets) > 0:
                            mandatory_net = mandatory_nets[0]
                            if isinstance(mandatory_net, list) and len(mandatory_net) > 1:
                                log.warning(
                                    f"Warning in map_event distance: list contains more than 1 element {mandatory_nets}")
                            if isinstance(mandatory_net, dict) and mandatory_net.get('count', None):
                                mandatory_net_counts = mandatory_net['count']
                                if isinstance(mandatory_net_counts, list) and len(mandatory_net_counts) > 0:
                                    distance_item['mandatoryNet'] = mandatory_net_counts[0]
                    if isinstance(distance, dict) and distance.get('optionalNet'):
                        optional_nets = distance['optionalNet']
                        if isinstance(optional_nets, list) and len(optional_nets) > 0:
                            optional_net = optional_nets[0]
                            if isinstance(optional_net, list) and len(optional_net) > 1:
                                log.warning(
                                    f"Warning in map_event distance: list contains more than 1 element {optional_net}")
                            if isinstance(optional_net, dict) and optional_net.get('count', None):
                                optional_net_counts = optional_net['count']
                                if isinstance(optional_net_counts, list) and len(optional_net_counts) > 0:
                                    distance_item['optionalNet'] = optional_net_counts[0]

                    event['distance'] = distance_item
                else:
                    log.warning(f"Warning in map_event distance: unexpected type {distance}")

            if event_content.get('lastEdited', None) and self.valid_date_format(event_content['lastEdited']):
                event['lastEdited'] = str(event_content.get('lastEdited', None))
            if event_content.get('paceOfStudy', None):
                pace_of_study = event_content['paceOfStudy']
                if pace_of_study.get('percentage', None):
                    event['paceOfStudyPercentage'] = pace_of_study['percentage']
            if event_content.get('url', None):
                urls = event_content['url']
                if urls.get('url', None):
                    event_urls = urls['url']
                    lang_content_arr = []
                    if isinstance(event_urls, list):
                        for event_url in event_urls:
                            lang_content_item = LangContent()
                            lang_content_item['lang'] = event_url.get('lang', None)
                            lang_content_item['content'] = event_url.get('content', None)
                            lang_content_arr.append(lang_content_item)
                        event['urls'] = lang_content_arr
                    else:
                        log.warning(f"Warning in map_event url: unexpected type {event_urls}")
            if event_content.get('application', None):
                event['application'] = self.map_application(event_content['application'])

            if event_content.get('languageOfInstruction'):
                language_of_instructions = event_content['languageOfInstruction']
                if isinstance(language_of_instructions, list) and len(language_of_instructions) > 0:
                    event['languageOfInstruction'] = language_of_instructions[0]
                    if isinstance(language_of_instructions, list) and len(language_of_instructions) > 1:
                        log.warning(
                            f"Warning in map_event language_of_instructions: list contains more than 1 element {language_of_instructions}")
                elif not isinstance(language_of_instructions, list):
                    event['languageOfInstruction'] = language_of_instructions
            if event_content.get('provider'):
                providers = event_content['provider']
                if isinstance(providers, list) and len(providers) > 0:
                    event['provider'] = providers[0]
                    if len(providers) > 1:
                        log.warning(f"Warning in map_event providers: list contains more than 1 element {providers}")
                elif not isinstance(providers, list):
                    event['provider'] = providers
            event['timeOfStudy'] = self.map_code_type(event_content.get('timeOfStudy', None))
            event['locations'] = self.map_locations(event_content.get('location', None))
        except Exception as e:
            log.debug(f"Error in map_event with input: {event_content}")
            log.error(e)
        return event

    def parse(self, response):
        log.info('Parsing infos...')
        # Parse education infos when events and education_providers are cached...
        json_response = json.loads(response.body)
        if json_response.get('content'):
            for education in json_response.get('content'):
                event_items = []
                events = []
                id = education['content']['educationInfo']['identifier'].lower()

                if id not in self.scraped_educations:
                    log.debug(f'Parsing education with id: {id}')
                    unique_education_providers = []
                    if self.cached_events.get(id):
                        events = self.cached_events[id]
                        for event in events:
                            event_items.append(self.map_event(event))
                            provider_ids = event['provider']
                            for provider_id in provider_ids:
                                provider_id = provider_id.lower()
                                if self.cached_education_providers.get(provider_id):
                                    already_added = False
                                    for unique_provider in unique_education_providers:
                                        # Check if provider already exist in education...
                                        if unique_provider.get('identifier', None).lower() == provider_id:
                                            already_added = True
                                            break
                                    if not already_added:
                                        unique_education_providers.append(
                                            self.map_provider(self.cached_education_providers.get(provider_id)))
                    education_item = self.map_education(education)

                    # Skip educations with blacklisted education_form:
                    skip_education = False
                    if education_item.get('form'):
                        education_form = education_item['form']
                        if education_form.get('code'):
                            education_form_code = education_form['code']
                            if education_form_code in self.BLACKLIST_EDUCATION_FORMS:
                                if education_form_code not in self.not_scraped_aub_types:
                                    log.info(f"Found NOT valid education_form_code: {education_form_code}")
                                    self.not_scraped_aub_types.add(education_form_code)
                                log.debug(f"Skip education {id} with blacklisted form: '{education_form_code}'")
                                skip_education = True
                            else:
                                self.scraped_aub_types.add(education_form_code)

                    if not skip_education:
                        self.scraped_educations.add(id)

                        yield {
                            'id': id,
                            'education': education_item,
                            'events': event_items,
                            'education_providers': unique_education_providers
                        }

        yield from self._follow_next_infos_page(response)

    def parse_events(self, response):
        log.info('Parsing events...')
        json_response = json.loads(response.body)

        events_to_store = json_response.get('content')
        # Add events to hashmap (i.e. dict) using education_id as key...
        for event in events_to_store:
            try:
                education_id = event['content']['educationEvent']['education'].lower()
                if self.cached_events.get(education_id, None):
                    events_for_key = self.cached_events[education_id]
                    event_id = event['content']['educationEvent']['identifier']
                    event_already_exist = False
                    for event_for_key in events_for_key:
                        if event_for_key.get('identifier') == event_id:
                            event_already_exist = True

                    if not event_already_exist:
                        events_for_key.append(event['content']['educationEvent'])
                        self.cached_events[education_id] = events_for_key

                else:
                    self.cached_events[education_id] = [event['content']['educationEvent']]
            except Exception as e:
                log.error('Error in parse_events:')
                log.error(e)
        yield from self._follow_next_events_page(response)

    def start_providers_scraping(self):
        log.info('Starting to scrape providers...')
        url = self.API_URL + (self.PROVIDERS_URL_PATTERN % (self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[0]))
        yield scrapy.http.Request(url, headers=self.headers, callback=self.parse_providers,
                                  meta={'organization_form_index': 0})

    def parse_providers(self, response):
        log.info('Parsing providers...')
        json_response = json.loads(response.body)
        providers_to_store = json_response.get('content')
        for provider in providers_to_store:
            try:
                provider_id = provider['content']['educationProvider']['identifier'].lower()
                self.cached_education_providers[provider_id] = provider['content']['educationProvider']
            except Exception as e:
                log.debug(f"Error in parse_providers with input: {response}")
                log.error(e)
        yield from self._follow_next_providers_page(response)

    def _follow_next_providers_page(self, response):
        json_response = json.loads(response.body)
        page = json_response.get('page', None)
        organization_form_index = response.meta.get('organization_form_index')
        if page:
            current_page = int(page.get('number', None))
            total_pages = int(page.get('totalPages', None))


            log.info(f'Parsing providers page {current_page}/{total_pages} for organization form: {self.organization_forms_to_scrape[organization_form_index]}')
            if current_page < total_pages:
                next_page = response.urljoin((self.PROVIDERS_URL_PATTERN % (
                self.ITEMS_PER_PAGE, str(current_page + 1), self.organization_forms_to_scrape[organization_form_index])))
                yield scrapy.http.Request(next_page, callback=self.parse_providers,
                                          meta={'organization_form_index': organization_form_index})
            else:
                next_organization_form_index = organization_form_index + 1
                if next_organization_form_index < len(self.organization_forms_to_scrape):
                    next_page = response.urljoin((self.PROVIDERS_URL_PATTERN % (
                    self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[next_organization_form_index])))
                    yield scrapy.http.Request(next_page, callback=self.parse_providers,
                                              meta={'organization_form_index': next_organization_form_index})
                else:
                    log.debug(f'Scraped {len(self.cached_events)} providers and saved in cache')
                    yield from self.start_subjects_scraping()

    def start_subjects_scraping(self):
        log.info('Starting to scrape subjects...')
        url = self.API_URL + (self.SUBJECTS_URL_PATTERN % (self.ITEMS_PER_PAGE, 0))
        yield scrapy.http.Request(url, headers=self.headers, callback=self.parse_subjects)

    def parse_subjects(self, response):
        log.info('Parsing subjects...')
        json_response = json.loads(response.body)
        subjects_to_store = json_response.get('content')
        for subject in subjects_to_store:
            try:
                subject_code = str(subject['code'])
                self.cached_subjects[subject_code] = subject
            except Exception as e:
                log.debug(f"Error in parse_subjects with input: {response}")
                log.error(e)
        yield from self._follow_next_subjects_page(response)

    def _follow_next_subjects_page(self, response):
        # Step to next page using page request param
        json_response = json.loads(response.body)
        page = json_response.get('page', None)
        if page:
            current_page = int(page.get('number', None))
            total_pages = int(page.get('totalPages', None))
            log.info(f'Parsing subjects page {current_page}/{total_pages}')
            if current_page < total_pages:
                next_page = response.urljoin((self.SUBJECTS_URL_PATTERN % (self.ITEMS_PER_PAGE, str(current_page + 1))))
                yield scrapy.http.Request(next_page, callback=self.parse_subjects)
            else:
                log.debug(f'Scraped {len(self.cached_subjects)} subjects and saved in cache')
                yield from self.start_infos_scraping()

    def _follow_next_events_page(self, response):

        json_response = json.loads(response.body)
        page = json_response.get('page', None)
        organization_form_index = response.meta.get('organization_form_index')
        if page:
            current_page = int(page.get('number', None))
            total_pages = int(page.get('totalPages', None))

            log.info(f'Parsing events page {current_page}/{total_pages} for organization form: {self.organization_forms_to_scrape[organization_form_index]}')
            if current_page < total_pages:
                next_page = response.urljoin((self.EVENTS_URL_PATTERN % (
                self.ITEMS_PER_PAGE, str(current_page + 1), self.organization_forms_to_scrape[organization_form_index])))
                yield scrapy.http.Request(next_page, callback=self.parse_events,
                                          meta={'organization_form_index': organization_form_index})
            else:
                next_organization_form_index = organization_form_index + 1
                if next_organization_form_index < len(self.organization_forms_to_scrape):
                    next_page = response.urljoin((self.EVENTS_URL_PATTERN % (
                    self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[next_organization_form_index])))
                    yield scrapy.http.Request(next_page, callback=self.parse_events,
                                              meta={'organization_form_index': next_organization_form_index})
                else:
                    log.debug(f'Scraped {len(self.cached_events)} events and saved in cache')
                    yield from self.start_providers_scraping()

    def start_infos_scraping(self):
        log.info('Starting to scrape infos...')
        url = self.API_URL + (self.INFOS_URL_PATTERN % (self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[0]))
        yield scrapy.http.Request(url, headers=self.headers, callback=self.parse, meta={'organization_form_index': 0})

    def _follow_next_infos_page(self, response):
        json_response = json.loads(response.body)
        page = json_response.get('page', None)
        organization_form_index = response.meta.get('organization_form_index')
        if page:
            current_page = int(page.get('number', None))
            total_pages = int(page.get('totalPages', None))

            log.info(f'Parsing infos page {current_page}/{total_pages} for organization form: {self.organization_forms_to_scrape[organization_form_index]}')
            if current_page < total_pages:
                next_page = response.urljoin((self.INFOS_URL_PATTERN % (
                self.ITEMS_PER_PAGE, str(current_page + 1), self.organization_forms_to_scrape[organization_form_index])))
                yield scrapy.http.Request(next_page, callback=self.parse,
                                          meta={'organization_form_index': organization_form_index})
            else:
                next_organization_form_index = organization_form_index + 1
                if next_organization_form_index < len(self.organization_forms_to_scrape):
                    next_page = response.urljoin((self.INFOS_URL_PATTERN % (
                    self.ITEMS_PER_PAGE, 0, self.organization_forms_to_scrape[next_organization_form_index])))
                    yield scrapy.http.Request(next_page, callback=self.parse,
                                              meta={'organization_form_index': next_organization_form_index})
