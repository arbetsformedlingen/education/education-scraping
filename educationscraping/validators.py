from schematics.models import Model
from schematics.types import URLType, StringType, ListType, DateType, ModelType, NumberType, PolyModelType


class addressValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    name = StringType(required=False)
    address = StringType(required=False)
    postalCode = StringType(required=False)
    municipality = StringType(required=False)
    streetAddress = StringType(required=False)
    addressLocality = StringType(required=False)

class hiringOrganizationValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    url = StringType(required=False)
    name = StringType(required=False)
    logo = StringType(required=False)
    description = StringType(required=False)
    address = ModelType(addressValidation, required=False)

class jobLocationValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    name = StringType(required=False)
    address = StringType(required=False)

class occupationalCategoryValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    codeValue = StringType(required=False)

class relevantOccupationValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    name = StringType(required=False)
    occupationalCategory = ModelType(occupationalCategoryValidation, required=False)

class jobLocationValidationGronajobb(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    address = ModelType(addressValidation, required=False)

class hiringOrganizationValidationArbetsformedlingenRestWS(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    url = StringType(required=False)
    name = StringType(required=False)
    address = StringType(required=False)

class jobLocationValidationArbetsformedlingenRestWS(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    city = StringType(required=False)
    postalCode = StringType(required=False)
    municipality = StringType(required=False)
    streetAddress = StringType(required=False)
    addressLocality = StringType(required=False)
    address = ModelType(addressValidation, required=False)

class JobPostingValidation(Model):
    Model.fields["@type"] = StringType(required=False)
    Model.fields["@context"] = StringType(required=False)

    url = URLType(required=True)
    title = StringType(required=True)
    datePosted = DateType(required=False)
    industry = StringType(required=False)
    workHours = NumberType(required=False)
    description = StringType(required=True)
    identifier = StringType(required=False)
    validThrough = DateType(required=False)
    baseSalary = NumberType(required=False)
    employmentType = StringType(required=False)
    totalJobOpenings = StringType(required=False)
    skills = ListType(StringType, required=False)
    relevantOccupation = StringType(required=False)
    jobBenefits = ListType(StringType, required=False)
    experienceRequirements = StringType(required=False)
    qualifications = ListType(StringType, required=False)
    responsibilities = ListType(StringType, required=False)
    occupationalCategory = ListType(StringType, required=False)
    educationRequirements = ListType(StringType, required=False)
    jobLocation = ModelType(jobLocationValidation, required=False)
    hiringOrganization = ModelType(hiringOrganizationValidation, required=False)

class ExtendedJobPostingValidation(JobPostingValidation):
    keywords = ListType(StringType, required=False)

class ExtendedValidationGronajobb(JobPostingValidation):
    jobLocation = ModelType(jobLocationValidationGronajobb, required=False)

class ExtendedValidationStudentjob(JobPostingValidation):
    jobLocation = ListType(ModelType(jobLocationValidation), required=False)
    employmentType = ListType(StringType, required=False)

class ExtendedValidationStepstone(JobPostingValidation):
    jobLocation = StringType(required=False)
    relevantOccupation = ListType(ModelType(relevantOccupationValidation), required=False)

class ExtendedValidationOnepartnergroup(JobPostingValidation):
    occupationalCategory = StringType(required=False)

class ExtendedValidationOffentligajobb(JobPostingValidation):
    relevantOccupation = ModelType(relevantOccupationValidation, required=False)

class ExtendedValidationNetjobs(JobPostingValidation):
    jobLocation = ListType(ModelType(jobLocationValidation), required=False)

class ExtendedValidationArbetsformedlingenRestWS(JobPostingValidation):
      datePosted = StringType(required=False)
      validThrough = StringType(required=False)
      relevantOccupation = ModelType(relevantOccupationValidation, required=False)
      hiringOrganization = ModelType(hiringOrganizationValidationArbetsformedlingenRestWS, required=False)
      jobLocation = ModelType(jobLocationValidationArbetsformedlingenRestWS, required=False)















