import os

from scrapy.utils.project import get_project_settings

from educationscraping.spiders.educationplans.stockholms_universitet import (
    StockholmsUniversitetSpider,
)
from educationscraping.spiders.educationplans.umea_universitet import (
    UmeaUniversitetSpider,
)
from educationscraping.spiders.educationplans.uppsala_universitet import (
    UppsalaUniversitetSpider,
)
from educationscraping.spiders.educations.susanavet import SusanavetSpider

# from educationscraping.spiders.educationplans.malardalens_universitet import malardalens_universitet
from educationscraping.spiders.educationplans.linkopings_universitet import (
    LinkopingsUniversitetSpider,
)
from educationscraping.spiders.educationplans.myh_kvalifikationer import (
    MyhKvalifikationerSpider,
)

from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

os.environ["SCRAPY_SETTINGS_MODULE"] = "educationscraping.settings"

settings = get_project_settings()
configure_logging()
runner = CrawlerRunner(settings)


@defer.inlineCallbacks
def crawl():
    # Crawl spiders sequentially...
    yield runner.crawl(SusanavetSpider)
    yield runner.crawl(LinkopingsUniversitetSpider)
    yield runner.crawl(StockholmsUniversitetSpider)
    yield runner.crawl(UppsalaUniversitetSpider)
    yield runner.crawl(UmeaUniversitetSpider)
    yield runner.crawl(MyhKvalifikationerSpider)
    # yield runner.crawl(malardalens_universitet)
    # Add more spiders here...

    reactor.stop()


crawl()
reactor.run()  # the script will block here until the last crawl call is finished
