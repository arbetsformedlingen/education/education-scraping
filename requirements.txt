boto3==1.35.6
beautifulsoup4~=4.9.3
certifi~=2024.7.4
cryptography==43.0.1
dateparser==1.0.0
extruct==0.11.0
jmespath==1.0.1
price-parser==0.3.4
pytest==7.2.0
pytest-cov==4.0.0
requests~=2.32.3
scrapy==2.11.2
scrapy-crawlera==1.7.2
spidermon~=1.15.0
schematics~=2.1.0
setuptools>=73.0.1 # not directly required, pinned by Snyk to avoid a vulnerability
urllib3~=1.26.13
