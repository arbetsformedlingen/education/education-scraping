import logging
import os
import pytest
from educationscraping.html_utils import clean_html, limit_linebreaks, clean_cdata

log = logging.getLogger(__name__)

@pytest.mark.parametrize("input_string, expected_output, comment", [
    ('En mening utan html fast med radbrytning\nAndra raden kommer här',
     'En mening utan html fast med radbrytning\nAndra raden kommer här', ''),
    ('<b>Rubrik</b>Rad 1<br />Rad 2', 'RubrikRad 1\nRad 2', 'br'),
    ('<b>Rubrik</b>Rad 1<br /><br /><br /><br />Rad 2', 'RubrikRad 1\n\nRad 2', 'multiple br'),
    ('<p>Paragraf 1</p><p>Paragraf 2</p>', 'Paragraf 1\n\nParagraf 2', 'p tags'),
    ('<p>Paragraf 1<p>Nästlad paragraf</p>Och fortsättning paragraf 1</p><p>Paragraf 2</p>',
     'Paragraf 1\nNästlad paragraf\nOch fortsättning paragraf 1\n\nParagraf 2', 'nested p tags'),
    (
            '<p><strong>DIN ROLL:</strong></p><ul><li>Helhetsansvar för implementationsprocessen.</li><li>Planläggning och projektledning.</li><li>Analys och migrering av kundens data.</li></ul>Efter lista',
            'DIN ROLL:\nHelhetsansvar för implementationsprocessen.\nPlanläggning och projektledning.\nAnalys och migrering av kundens data.\nEfter lista',
            'ul and li tags'),
    ('<p>Paragraf 1 utan sluttag.', 'Paragraf 1 utan sluttag.', 'non valid html'),
    ('Paragraf 1 utan starttag.</p>', 'Paragraf 1 utan starttag.', 'non valid html'),
    ('brtag som inte är<br>xhtml', 'brtag som inte är\nxhtml', 'non valid html'),
    ('<h1>Helt fel rubrik</h2>', 'Helt fel rubrik', 'non valid html'),
    ('<h1 hittepå="test">Eget attribut</h1>', 'Eget attribut', 'non valid html'),
    ('<script>alert("test");</script>', '', 'clean script tags'),
    ('<script>alert("test");</script>Lite text. Lite till. <script>alert("test");</script>Och lite till.',
     'Lite text. Lite till. Och lite till.', 'clean script tags'),
    (None, '', 'check for None'),
    ('<b>Dina arbetsuppgifter</b><p>Du kommer att undervisa inom ämnet trädgård.',
     'Dina arbetsuppgifter\nDu kommer att undervisa inom ämnet trädgård.', 'non valid p tags'),
    ('Dina arbetsuppgifter<p>Du kommer att undervisa inom ämnet trädgård.',
     'Dina arbetsuppgifter\nDu kommer att undervisa inom ämnet trädgård.', 'p tags no previous sibling'),
    ('<p><b>Dina arbetsuppgifter</b></p><p>Du kommer att undervisa inom ämnet trädgård.',
     'Dina arbetsuppgifter\n\nDu kommer att undervisa inom ämnet trädgård.', 'double p tags'),
    ('Staffing &amp; Recruitment Assistant? ', 'Staffing & Recruitment Assistant?', 'html encoded &'),
    ('the world&#39;s largest', "the world's largest", 'html encoded apostrofe'),
    ('på &quot;eget&quot; apotek', 'på "eget" apotek', 'html encoded quote'),

])
def test_clean_html(input_string, expected_output, comment):
    assert clean_html(input_string) == expected_output

def test_clean_html_headlines():
    for i in range(1, 7):
        input = '<h%s>Rubrik</h%s>Brödtext här' % (i, i)
        output = clean_html(input)
        assert ('Rubrik\nBrödtext här' == output)

def test_limit_linebreaks():
    output = limit_linebreaks('ord1\n\n\n\nord2')
    assert 'ord1\n\nord2' == output

@pytest.mark.parametrize("input_string, expected_output, comment", [
    ('<![CDATA[ Godkänt betyg i svenska/svenska som andraspråk på grundläggande nivå eller motsvarande kunskakper, B-körkort och god körvana. Du måste ha fyllt 21 år. ]]>',
     'Godkänt betyg i svenska/svenska som andraspråk på grundläggande nivå eller motsvarande kunskakper, B-körkort och god körvana. Du måste ha fyllt 21 år.', ''),
    ('<![CDATA[ Yrkesförare godstransport 2, lastbil CE, klassrum<div><div>Yrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.</div></div> ]]>',
     'Yrkesförare godstransport 2, lastbil CE, klassrum<div><div>Yrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.</div></div>',''),
    ('Yrkesförare godstransport 2, lastbil CE, klassrum<div><div>Yrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.</div></div>',
     'Yrkesförare godstransport 2, lastbil CE, klassrumYrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.', ''),
    ('Yrkesförare godstransport 2, lastbil CE, klassrum Yrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.',
     'Yrkesförare godstransport 2, lastbil CE, klassrum Yrkesförarutbildningen är för dig som gillar stora, tunga fordon och kan tänka dig en karriär inom den breda transportbranschen. Att köra lastbil i yrkestrafik är ett ansvarsfullt arbete som ställer höga krav på kunskap och skicklighet. Du läser därför mycket om olika fordon, trafikregler, lagar och förordningar som gäller för transport av gods samt arbetsmiljö och säkerhet.', '')

])
def test_clean_html_cdata(input_string, expected_output, comment):
    output = clean_cdata(input_string)
    assert expected_output == output

if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])
    # pytest.main([os.path.realpath(__file__), '-svv', '-ra', '-m unit'])
