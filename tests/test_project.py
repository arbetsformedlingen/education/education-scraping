from scrapy.utils.project import get_project_settings


def test_bot_name():
    settings = get_project_settings()
    assert 'arbetsformedlingen' == settings.get('BOT_NAME')
