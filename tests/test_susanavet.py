import pytest
from educationscraping.spiders.educations.susanavet import SusanavetSpider


@pytest.mark.parametrize("input, expected", [
  ('1234', True),
  ('0123', True),
  (1234, True),
  (0, False),
  (123, False),
  (12345, False),
  ('abcd', False),
  ('12e4', False),
  ([1, 2, 3, 4], False)
  ])
def test_valid_municipaliy_code(input, expected):
  assert SusanavetSpider.valid_municipaliy_code(input) == expected